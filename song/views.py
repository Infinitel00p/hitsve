from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, Http404
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
# Create your views here.
from .forms import *
from .models import *
from django.contrib.messages.api import success
from django.urls.base import reverse_lazy
# Create your views here.
from web.models import UserArtist, Usuario

from django.contrib.auth.decorators import permission_required, login_required

from django.db.models import Q

import pafy

# -------- Administrador --------------    
def createSongAdmin(request):
    form = SongAdminForm(request.POST or None, request.FILES or None)
    form.fields['genre'].required = False
    form.fields['artists'].required = False
    
    title = "Nueva Canción"
    action = "/player/nueva"

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/admin/create.html', {'form':form, 'album_id':0,  'photo':"", 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get":True,
                "url":"/player/nueva"
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/admin/create.html', {'form':form, 'album_id':0, 'photo':"", 'title':title, 'action':action})
    else:
        album_id = request.POST.get('album')

        if form.is_valid():
            song = form.save(commit= False)

            artists = form.cleaned_data['artists']
            name = form.cleaned_data['name']

            x = Song.objects.filter(name__iexact = name ,artists__exact = artists)
            
            if (x.first() is not None ):
                if (artists.count() == x.first().artists.all().count()):
                    h = True
                    for a in x.first().artists.all():
                        for b in artists:
                            print(str(b) + str(b != a) + str(a))
                            if b != a :
                                h = False
                            else:
                                h = True
                                break
                    if (h):
                        mensaje = "Esta canción ya existe"
                        if (request.is_ajax()):
                            response = {
                                "html" : render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id, 'photo':"",'mensaje':mensaje, 'title':title,'action':action}).getvalue().decode("utf-8") ,
                                "success" : True
                            }
                            return JsonResponse(response, safe=False) 
                        else:
                            return render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id, 'photo':"", 'mensaje':mensaje, 'title':title, 'action':action})
                    


            if album_id != "0":
                album = Album.objects.get(id = album_id)
                song.album = album

            try:
                song.duration = timedelta(seconds=pafy.new(song.id_youtube).length)
            except Exception as e:
                if (request.is_ajax()):
                    response = {
                        "html" : render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id, 'mensaje':'El id de youtube no existe', 'title':title,'action':action}).getvalue().decode("utf-8") ,
                        "success" : True
                    }
                    return JsonResponse(response, safe=False) 
                else:
                    return render(request, 'song/admin/create.html', {'form':form, 'album':album_id, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action})
      
                
            song.save()
            artists_id_list = form.cleaned_data['artists']
            song.artists = artists_id_list
            
            return redirect('player:listar_canciones')
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id,  'photo':"", 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id,  'photo':"", 'title':title, 'action':action})
        

def updateSongAdmin(request, id):
    try:
        song = Song.objects.get(id = id)
    except  :
        return redirect('player:listar_canciones')
    

    title = "Editar {0}".format(song.name)
    action = "/player/editar/{0}".format(song.id)

    if request.method == 'GET':
        form = SongAdminForm(instance=song)
        form.fields['photo'].required = False
        form.fields['genre'].required = False
        form.fields['artists'].required = False
    
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/admin/create.html', {'form':form, 'album_id':0,  'photo':song.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/admin/create.html', {'form':form, 'album_id':0, 'photo':song.photo, 'title':title, 'action':action })
    else:
        form = SongAdminForm(request.POST or None, request.FILES or None, instance = song)
        form.fields['photo'].required = False
        form.fields['genre'].required = False
        form.fields['artists'].required = False
    
        album_id=request.POST.get('album')
        
        if form.is_valid():
            song = form.save(commit=False)
            
            if album_id != "0":
                album = Album.objects.get(id = album_id)
                song.album = album
                
            artists_id_list = form.cleaned_data['artists']
            song.artists = artists_id_list
            
            try:
                song.duration = timedelta(seconds=pafy.new(song.id_youtube).length)
            except Exception as e:
                if (request.is_ajax()):
                    response = {
                        "html" : render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id,  'mensaje':'El id de youtube no existe', 'title':title,'action':action}).getvalue().decode("utf-8") ,
                        "success" : True
                    }
                    return JsonResponse(response, safe=False) 
                else:
                    return render(request, 'song/admin/create.html', {'form':form, 'album':album, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action})
      
            
            song.save()
            
            return redirect('player:listar_canciones')
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id,  'photo':song.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/admin/create.html', {'form':form, 'album_id':album_id, 'photo':song.photo, 'title':title, 'action':action})
        
        
        
from django.http import JsonResponse
from django.core import serializers

def get_albums(request):
    artists_id_list = request.GET.getlist('artists[]')
    albums = []
    a = []
    for id in artists_id_list :
        a = Album.objects.filter(artist = id)
        for album in a:
            b = {'id': album.id, 'name':album.name}
            albums.append(b)
           
    #albums_serialized = serializers.serialize('json', albums)
    return JsonResponse(albums, safe=False)



from django.db.models import Q
from django.db.models import Count

def listSong(request):
    
    if (request.method == "GET"):


        search = request.GET.get("search")

        if (search != "" and search is not None):
            query = Song.objects.annotate(num_views=Count('view')).filter(Q(artists__name__icontains=search) | Q(name__icontains=search) | Q(album__name__icontains=search) | Q(id_youtube=search)).filter(artists__artist=None).order_by('-release_date')
        else:
            query = Song.objects.annotate(num_views=Count('view')).filter(artists__artist=None).order_by('-release_date')
        
        if ( query is not None):
            page = request.GET.get('page', 1)

            paginator = Paginator(query, 12)

            try:
                objs = paginator.page(page)
            except PageNotAnInteger:
                objs = paginator.page(1)
            except EmptyPage:
                objs = paginator.page(paginator.num_pages)
        else:
            objs = query

        if (request.is_ajax()):
            response = {
                "html" : render(request, "song/admin/listSong.html", {'object_list':objs}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":"/player/lista"
            }

            return JsonResponse(response, safe=False)
        else:
            return render(request, "song/admin/listSong.html", {'object_list':objs})

    else:
        HttpResponseBadRequest()

def listAlbum(request):
    if (request.method == "GET"):
        objs = Album.objects.filter(artist__artist=None)
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/admin/listAlbum.html', {'object_list':objs}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":"/player/album/lista"
            }

            return JsonResponse(response, safe=False)
        else:
            return render(request, 'song/admin/listAlbum.html', {'object_list':objs})

    else:
        HttpResponseBadRequest()


def albumList(request, id):

    try:
        album = Album.objects.get(id = id)
        relacionados = Album.objects.all().order_by('?')[:3]
    except Exception as e:
        return HttpResponseBadRequest()

    return render(request, "song/album/detail.html", {'album':album, 'relacionados':relacionados})

def playList(request, id):

    try:
        playlist = PlayList.objects.get(id = id)
        relacionados = PlayList.objects.all().order_by('?')[:3]
    except Exception as e:
        return HttpResponseBadRequest()

    return render(request, "song/playlist/detail.html", {'playlist':playlist, 'relacionados':relacionados})

# --------------------- Albums admin ----------------------

def createAlbumAdmin(request):
    form = AlbumForm(request.POST or None, request.FILES or None)
    form.fields['artist'].required = False
    
    title = "Nuevo album"
    action = "/player/album/nuevo"

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/album/createAdmin.html', {'form':form, 'photo':"",'title':title,'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/album/createAdmin.html', {'form':form, 'photo':"", 'title':title, 'action':action})
    else:

        if form.is_valid():
            album = form.save(commit= False)
    
            album.save()

            published = request.POST.get('value')
            if published == "a" :
                return redirect("/player/album/{0}/cancion".format(album.id))
            else :
                return redirect('player:listar_albums')

        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/album/createAdmin.html', {'form':form, 'photo':"",'title':title,'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/album/createAdmin.html', {'form':form, 'photo':"",'title':title,'action':action})
                

def updateAlbumAdmin(request, id):

    try:
        album = Album.objects.get(id=id)
    except Exception as e:
        return HttpResponseBadRequest()

    form = AlbumForm(request.POST or None, request.FILES or None, instance=album)
    form.fields['artist'].required = False
    
    title = "Editar {0}".format(album.name)
    action = "/player/album/editar/{0}".format(album.id)

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/album/createAdmin.html', {'form':form, 'photo':"",'title':title,'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/album/createAdmin.html', {'form':form, 'photo':album.photo, 'title':title,'action':action})
    else:

        if form.is_valid():
            album = form.save(commit= False)
    
            album.save()

            published = request.POST.get('value')
            if published == "a" :
                return redirect("/player/album/{0}/cancion".format(album.id))
            else :
                return redirect('player:listar_albums')

        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/album/createAdmin.html', {'form':form, 'photo':album.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/album/createAdmin.html', {'form':form, 'photo':album.photo, 'title':title, 'action':action})
                

def deleteAlbum(request, id):
    try:
        album = Album.objects.get(id = id)
        album.delete()
    except Exception as e:
        HttpResponseBadRequest()

    return redirect('player:listar_albums')

def createAlbumSongAdmin(request, id):

    try:
        album = Album.objects.get(id=id)
    except Exception as e:
        return HttpResponseBadRequest()

    form = AlbumSongAdminForm(request.POST or None, request.FILES or None)
    form.fields['genre'].required = False
    form.fields['artists'].required = False
    
    title = "Nueva canción"
    action = "/player/album/{0}/cancion".format(album.id)

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/album/createSongAdmin.html', {'form':form, 'album':album, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/album/createSongAdmin.html', {'form':form, 'album':album, 'title':title, 'action':action})
    else:

        if form.is_valid():
            song = form.save(commit= False)

            
            artists = form.cleaned_data['artists']
            name = form.cleaned_data['name']

            x = Song.objects.filter(name__iexact = name ,artists__exact = artists)
            if (x):
                if (artists.count() == x.first().artists.all().count()):
                    h = True
                    for a in x.first().artists.all():
                        for b in artists:
                            print(str(b) + str(b != a) + str(a))
                            if b != a :
                                h = False
                            else:
                                h = True
                                break
                    if (h):
                        mensaje = "Esta canción ya existe"
                        if (request.is_ajax()):
                            response = {
                                "html" : render(request, 'song/album/createSongAdmin.html', {'form':form, 'album':album, 'title':title,'action':action}).getvalue().decode("utf-8") ,
                                "success" : True
                            }
                            return JsonResponse(response, safe=False) 
                        else:
                            return render(request, 'song/album/createSongAdmin.html', {'form':form, 'album':album, 'title':title,'action':action})
        
            song.album = album
            song.photo = album.photo
            song.release_date = album.release_date
                
            try:
                song.duration = timedelta(seconds=pafy.new(song.id_youtube).length)
            except Exception as e:
                if (request.is_ajax()):
                    response = {
                        "html" : render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'mensaje':'El id de youtube no existe','action':action}).getvalue().decode("utf-8") ,
                        "success" : True
                    }
                    return JsonResponse(response, safe=False) 
                else:
                    return render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'mensaje':'El id de youtube no existe','action':action})

            song.save()
            artists_id_list = form.cleaned_data['artists']
            song.artists = artists_id_list


            published = request.POST.get('value')
            if published == "a" :
                return redirect("/player/album/{0}/cancion".format(album.id))
            else :
                return redirect('player:listar_canciones')

            
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/album/createSongAdmin.html', {'form':form, 'album':album, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/album/createSongAdmin.html', {'form':form, 'album':album, 'title':title, 'action':action})
        
#------------------------ albums User ---------------------
def createAlbumUser(request):

    try:
        user = UserArtist.objects.get(username=request.user.username) 
    except  :
        return HttpResponseBadRequest()

    form = AlbumUserForm(request.POST or None, request.FILES or None)

    title = "Nuevo album"
    action = "/player/album/new"

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/album/createUser.html', {'form':form, 'photo':"", 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/album/createUser.html', {'form':form, 'photo':"", 'title':title, 'action':action})
    else:

        if form.is_valid():
            album = form.save(commit= False)
            album.artist = user.singer
            album.save()

            published = request.POST.get('value')
            if published == "a" :
                return redirect("/player/album/{0}/new".format(album.id))
            else :
                return redirect("/usuarios/profile/{0}/albums/{1}".format(user.username,album.id))

        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/album/createUser.html', {'form':form, 'photo':"",'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/album/createUser.html', {'form':form, 'photo':"",'title':title, 'action':action})


def updateAlbumUser(request, id):

    try:
        user = UserArtist.objects.get(username=request.user.username)        
        album = Album.objects.get(id=id) 
    except  :
        return HttpResponseBadRequest()

    if (album.artist != user.singer):
        return HttpResponseBadRequest()

    form = AlbumUserForm(request.POST or None, request.FILES or None, instance=album)

    title = "Editar {0}".format(album.name)
    action = "/player/album/update/{0}".format(album.id)

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/album/createUser.html', {'form':form, 'photo':album.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/album/createUser.html', {'form':form, 'photo':album.photo, 'title':title, 'action':action})
    else:

        if form.is_valid():
            album = form.save(commit= False)
            album.artist = user.singer
            album.save()

            published = request.POST.get('value')
            if published == "a" :
                return redirect("/player/album/{0}/new".format(album.id))
            else :
                return redirect('player:listar_canciones')

        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/album/createUser.html', {'form':form, 'photo':album.photo,'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/album/createUser.html', {'form':form, 'photo':album.photo, 'title':title, 'action':action})



def deleteAlbumUser(request, id):
    try:
        album = Album.objects.get(id = id)
        if (album.artist.artist == UserArtist.objects.get(username=request.user.username)):
            album.delete()
            return redirect('/usuarios/profile/{0}/albums'.format(request.user.username))
        else:
            return HttpResponseBadRequest()
    except Exception as e:
        return HttpResponseBadRequest()

def deleteAlbumSongUser(request, ida, ids):
    try:
        album = Album.objects.get(id = ida)
        song = Song.objects.get(id = ids)
        if (album.artist.artist.username == request.user.username):
            if (song.album == album):
                song.album = None
                song.save()
            return redirect('/usuarios/profile/{0}/albums/{1}'.format(request.user.username,ida))
        else:
            return HttpResponseBadRequest()
    except Exception as e:
        return HttpResponseBadRequest()        


from datetime import timedelta
def createAlbumSongUser(request, id):
    try:
        user = UserArtist.objects.get(username=request.user.username) 
        album = Album.objects.get(id=id)
    except Exception as e:
        return HttpResponseBadRequest()

    if (album.artist != user.singer):
        return HttpResponseBadRequest()
    
    form = AlbumSongArtistForm(request.POST or None, request.FILES or None)
    form.fields['genre'].required = False

    title = "Nueva canción"
    action = '/player/album/{0}/new'.format(album.id)

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'title':title, 'action':action})
    else:

        if form.is_valid():
            song = form.save(commit= False)

            song.album = album
            song.photo = album.photo
            song.release_date = album.release_date
                
            song.file = form.cleaned_data['file']
            song.link_download = song.file

            try:
                song.duration = timedelta(seconds=pafy.new(song.id_youtube).length)
            except Exception as e:
                if (request.is_ajax()):
                    response = {
                        "html" : render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                        "success" : True
                    }
                    return JsonResponse(response, safe=False) 
                else:
                    return render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action})
      

            song.save()
            song.artists.add(user.singer)


            published = request.POST.get('value')
            if published == "a" :
                return redirect("/player/album/{0}/new".format(album.id))
            else :
                return redirect('/usuarios/profile/{0}/albums'.format(user.username))

        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/album/createSongUser.html', {'form':form, 'album':album, 'title':title, 'action':action})
      

#------------------------ User Artist ----------------------
    
def createSongUser(request):
    try:
        user = UserArtist.objects.get(username=request.user.username) 
    except  :
        return HttpResponseBadRequest()
    
    form = SongArtistForm(request.POST or None, request.FILES or None)
    form.fields['genre'].required = False

    title = "Nueva canción"
    action = '/player/new'

    if request.method == 'GET':
        
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/user/create.html', {'form':form, 'album_id':0, 'photo':"", 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/user/create.html', {'form':form, 'album_id':0, 'photo':"", 'title':title, 'action':action})
    else:
        album_id = request.POST.get('album')

        if form.is_valid():
            song = form.save(commit= False)
            
            if album_id != "0":
                album = Album.objects.get(id = album_id)
                song.album = album
                
                
            song.file = form.cleaned_data['file']
            song.link_download = song.file

            try:
                song.duration = timedelta(seconds=pafy.new(song.id_youtube).length)
            except Exception as e:
                if (request.is_ajax()):
                    response = {
                        "html" : render(request, 'song/user/create.html', {'form':form, 'album':album_id, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                        "success" : True
                    }
                    return JsonResponse(response, safe=False) 
                else:
                    return render(request, 'song/user/create.html', {'form':form, 'album':album_id, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action})
            
            song.save()
            song.artists.add(user.singer)

            return redirect('usuarios/profile/{0}/canciones'.format(user.username))
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/user/create.html', {'form':form, 'album_id':album_id,  'photo':"", 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/user/create.html', {'form':form, 'album_id':album_id,  'photo':"", 'title':title, 'action':action})


def updateSongUser(request, id):
    try:
        user = UserArtist.objects.get(username=request.user.username) 
        song = Song.objects.get(id = id)
    except  :
        return HttpResponseBadRequest()
    
    if (not (user.singer in song.artists.all())):
        return HttpResponseBadRequest()
    
    if song is None:
        return HttpResponseBadRequest()
    
    form = SongArtistForm(request.POST or None, request.FILES or None, instance=song)
    form.fields['photo'].required = False
    form.fields['file'].required = False
    form.fields['genre'].required = False
    
    title = "Editar {0}".format(song.name)
    action = '/player/update/{0}'.format(song.id)

    if request.method == 'GET':
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'song/user/create.html', {'form':form, 'album_id':0, 'photo':song.photo, 'title':title, 'action':action }).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'song/user/create.html', {'form':form, 'album_id':0, 'photo':song.photo, 'title':title, 'action':action })
    else:
        album_id=request.POST.get('album')
        
        if form.is_valid():
            song = form.save(commit= False)
            
            if album_id != "0":
                album = Album.objects.get(id = album_id)
                song.album = album

            song.file = form.cleaned_data['file']
            song.link_download = song.file

            try:
                song.duration = timedelta(seconds=pafy.new(song.id_youtube).length)
            except Exception as e:
                if (request.is_ajax()):
                    response = {
                        "html" : render(request, 'song/user/create.html', {'form':form, 'album':album, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                        "success" : True
                    }
                    return JsonResponse(response, safe=False) 
                else:
                    return render(request, 'song/user/create.html', {'form':form, 'album':album, 'mensaje':'El id de youtube no existe', 'title':title, 'action':action})
      
            song.save()
        
            song.artists.add(user.singer)
        
            return redirect('/usuarios/profile/{0}/canciones'.format(user.username))
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'song/user/create.html', {'form':form, 'album_id':album_id, 'photo':song.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'song/user/create.html', {'form':form, 'album_id':album_id, 'photo':song.photo, 'title':title, 'action':action})
        

def deleteSongUser(request, id):

    if request.is_ajax():

        try:
            user = UserArtist.objects.get(username=request.user.username) 
            song = Song.objects.get(id = id)


            if (not (user.singer in song.artists.all())):
                data = False
            else:
                song.delete()
                data = True

        except Exception as e:
            data = False
        
        return JsonResponse(data, safe = False)
    else:
        return HttpResponseBadRequest()
    
    
    
    



def get_albums_user(request):
    
    user = UserArtist.objects.get(username=request.user.username)
    albums = []
    a = Album.objects.filter(artist = user.singer)
    for album in a:
        b = {'id': album.id, 'name':album.name}
        albums.append(b)
   
           
    #albums_serialized = serializers.serialize('json', albums)
    return JsonResponse(albums, safe=False)


from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def like_song(request):

    id_song = request.POST.get('id_song')
    try:
        user = Usuario.objects.get(username=request.user.username)
        song = Song.objects.get(id = id_song)
        
        if (song.likes.all().filter(id = user.id).exists()):
            song.likes.remove(user)
            data = {
                'response': True,
                'like' : False
            }
        else:
            song.likes.add(user)
            data = {
                'response': True,
                'like' : True
            }
    except :
        data = {
            "response":False
        }
    
    return JsonResponse(data, safe = False)

   
def download(request, id):
    song = Song.objects.get(id=id)
    path = song.link_download
    file_path = os.path.join("", path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type='application/force-download')
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            print(response)
            sumDownload(song)
            return response
    raise Http404


from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
def detail(request, slug):
    try:
        song = Song.objects.get(slug=slug)

        relacionados = Song.objects.filter(genre__name__icontains=song.genre.name).order_by('?')[:6]
    except :
        return HttpResponseBadRequest()
        

    return render(request, 'song/user/post.html', {'cancion':song, 'relacionados':relacionados})


def sumDownload(song):
    song.num_downloads += 1

    song.save()
    return song.num_downloads

@csrf_exempt
def sum(request, id):
    try:
        song = Song.objects.get(id = id)

        if (request.user.is_authenticated()):
            user = Usuario.objects.get(username = request.user.username)
            song.downloads.add( user)

        n = sumDownload(song)
    except Exception as e:
        n = 0
    

    return JsonResponse(n, safe=False)


def get_link_download(request):
    id = request.GET.get("id")

    #id_you = Song.objects.get(id = id).id_youtube

    video = pafy.new(id)

    audio = video.m4astreams

    url = audio[0].url_https
    
    return JsonResponse(url, safe=False)


@csrf_exempt
def follow_artist(request):
    data = True
    
    id_artist = request.POST.get('id_artist')

    
    try:
        user = Usuario.objects.get(username=request.user.username)

        artist = Artist.objects.get(id = id_artist)
        
        if (artist.followers.filter(id = user.id).exists()):
            artist.followers.remove(user)
            if (hasattr(artist, 'artist')):
                user.remove_relationship(artist.artist,1)
            response = {
                "r" : True,
                "add" : False
            }
        else:
            artist.followers.add(user)
            if (hasattr(artist, 'artist')):
                user.add_relationship(artist.artist,1)
            response = {
                "r" : True,
                "add" : True
            }
    except :
        data = False
        response = {
            "r" : False,
        }

    return JsonResponse(response, safe = False)


def view_song(request):
    id_song = request.POST.get('id')

    try:
        song = Song.objects.get(id=id_song)
        view = View.objects.create(song = song, country= "guevo")
        data = True
    except Exception as e:
        data = False

    return JsonResponse(data, safe = False)

def view_playlist(request):
    id_playlist = request.POST.get('id')

    try:
        playlist = Song.objects.get(id=id_playlist)
        view = View.objects.create(song = playlist, country= "guevo")
        data = True
    except Exception as e:
        data = False
        
    return JsonResponse(data, safe = False)


#------------ playList ------------------
def createPlayListAjax(request):
    form = PlayListForm(request.POST or None, request.FILES or None)
    


    if request.method == 'GET':
        id_song = request.GET.get("id_song")
        print("-----" + str(id_song))
        lists = PlayList.objects.filter(user__username=request.user.username)
        return render(request, 'song/playlist/create.html', {'form':form, 'lists':lists, 'id_song':id_song})
    else:

        if request.is_ajax():

            playlist = form.save(commit= False)
            try:
                playlist.user = Usuario.objects.get(username=request.user.username)
                playlist.save()
                data = {
                    'error' : True,
                    'id' : str(playlist.id),
                    'name' : str(playlist.name) 
                }
            except Exception as e:
                data = {
                    'error': False,
                    'errorlist':'Escribe Algo!'
                }
            
            return JsonResponse(data, safe = False)
        else:
            return HttpResponseBadRequest()
        

def addToPlayListAjax(request):

    if request.is_ajax():

        try:
            playlist = PlayList.objects.get(id = request.POST.get('id_list'))
            song = Song.objects.get(id = request.POST.get('id_song') )
            playlist.songs.add(song)
            data = True
        except Exception as e:
            data = False
        
        return JsonResponse(data, safe = False)
    else:
        return HttpResponseBadRequest()


def deletePlayListSong(request, idp, ids):

    try:
        playlist = PlayList.objects.get(id = idp)
        song = Song.objects.get(id = ids)
        print(1)
        if (playlist.user.username == request.user.username):
            print(2)
            if (song in playlist.songs.all()):
                print(3)               
                playlist.songs.remove(song)
                print(5)
            return redirect('/usuarios/profile/{0}/playlists/{1}'.format(request.user.username,idp))
        else:
            print(4)
            return HttpResponseBadRequest()
    except Exception as e:
        return HttpResponseBadRequest()
        
    

def deletePlayList(request, id):

    try:
        playlist = PlayList.objects.get(id = id)
        if (playlist.user == Usuario.objects.get(username=request.user.username)):
            playlist.delete()
            data = True
              
            return redirect('/usuarios/profile/{0}/playlists'.format(request.user.username))
        else:
            return HttpResponseBadRequest()
            
    except Exception as e:
        return HttpResponseBadRequest()  
    
    


def createPlayList(request):
    form = PlayListForm(request.POST or None, request.FILES or None)
   
    if request.method == 'GET':
        return render(request, 'song/playlist/create.html', {'form':form})
    else:

        if request.is_ajax(): 

            playlist = form.save(commit= False)
            try:
                playlist.user = Usuario.objects.get(username=request.user.username)
                playlist.save()
                data = {
                    'error' : True,
                    'id' : str(playlist.id),
                    'name' : str(playlist.name) 
                }
            except Exception as e:
                data = {
                    'error': False,
                    'errorlist':'Escribe Algo!'
                }
            
            return JsonResponse(data, safe = False)
        else:
            return render(request, 'song/playlist/create.html', {'form':form})



def getPlayList(request):
    id = request.GET.get("id")
    try:
        playlist = PlayList.objects.get(id = id)
    except Exception as e:
        return HttpResponseBadRequest()

    data = []

    for cancion in playlist.songs.all():
        c = {
            'name': str(cancion.name),
            'duration': str(cancion.duration),
            'artists': str(" X ".join(cancion.artists.values_list('name', flat=True))),
            'id': str(cancion.id),
            'photo': str(cancion.photo),

        }
        data.append(c)

    #playlist_serialized = serializers.serialize('json', playlist.songs.all())
    return JsonResponse(data, safe = False)

def getAlbumSongs(request):
    id = request.GET.get("id")
    try:
        album = Album.objects.get(id = id)
    except Exception as e:
        return HttpResponseBadRequest()

    data = []

    for cancion in album.song_set.all():
        c = {
            'name': str(cancion.name),
            'duration': str(cancion.duration),
            'artists': str(" X ".join(cancion.artists.values_list('name', flat=True))),
            'id': str(cancion.id),
            'photo': str(cancion.photo),

        }
        data.append(c)

    #playlist_serialized = serializers.serialize('json', playlist.songs.all())
    return JsonResponse(data, safe = False)


#-------- comentarios ---------------

from django.utils.timesince import timesince
def comment(request):


    try:
        user = Usuario.objects.get(username=request.user.username) 
        song = Song.objects.get(id = request.POST.get('id'))
    except:
        return HttpResponseBadRequest()
    
    if request.is_ajax():    
        content = request.POST.get("content")

        if content:
            comment = Comment_Song();
            
            comment.author = user
            comment.song = song
            comment.content = content

            comment.save()
            comentario = {
                'error':True,
                'content':content,
                'time':str(timesince(comment.time_stamp)),
                'photo':str(comment.author.photo),
                'name':str(comment.author.username),
                'id':str(comment.author.pk),
            }
            #comentario_serialized = serializers.serialize('json', comentario)
            return JsonResponse(comentario, safe=False)
        else:
            error = {
                'error':False,
                'errorlist':'Escribe algo!!'
            }
            return JsonResponse(error, safe=False)


def comment_pag(request):

    id = request.GET.get('id')
    print(1)
    try:
        song = Song.objects.get(id=id)
    except :
        return HttpResponseBadRequest()

    page = request.GET.get('page', 1)

    paginator =  Paginator(song.comment_song_set.all().order_by("-time_stamp"), 10)

    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        comments = paginator.page(1)
    except EmptyPage:
        comments = paginator.page(paginator.num_pages)

    return render(request, 'song/user/comments_ajax.html', { 'comments':comments, 'song_id':id})



def delation(request):
    id = request.GET.get('s')
    song = Song.objects.get(id=id)

    if request.method == 'GET':
        return render(request, 'song/delation.html', {'song':song})
    else:
        return render(request, 'song/delation.html', {'song':song})


    
#Para La API

def api_get_link(request):
    id = request.GET.get("id")

    try:
        video = pafy.new(id)

        audio = video.m4astreams
        
        url = audio[0].url_https

        response = {
            "url" : url,
            "error": False
        }
    except Exception as e:
        response = {
            "url" : None,
            "error" : True
        }

    return JsonResponse(response, safe=False)





from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
import json

DEVELOPER_KEY = "AIzaSyCIM4EzNqi1in22f4Z3Ru3iYvLaY8tc3bo"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"
def new(request):

    q = request.GET.get("q")
    name = request.GET.get("name")

    if (q is not None):
        youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,developerKey=DEVELOPER_KEY)
        search_response = youtube.videos().list(
            id=q,
            part="id,snippet",
          ).execute()


        v = search_response.get("items", [])[0]
        n = v["snippet"]["title"].split('-')
        try:
            title = n[1]
            artist = n[0]
        except Exception as e:
            title = n[0]
            artist = ""
        video = {
            'id':v["id"],
            'title':title,
            'artist':artist,
            'thumb':v["snippet"]["thumbnails"]["high"]["url"]
        }


        search_response = youtube.search().list(
            type="video",
            part="snippet",
            maxResults=10,
            videoCategoryId=10,
            relatedToVideoId=q

          ).execute()
        relacionados = []

        for result in search_response.get("items", []):
            n = result["snippet"]["title"].split('-')
            try:
                title = n[1]
                artist = n[0]
            except Exception as e:
                title = n[0]
                artist = ""
                
            vr = {
                'id':result["id"]["videoId"],
                'title':title,
                'artist':artist,
                'name': result["snippet"]["title"],
                'channel': result["snippet"]["channelTitle"],
                'thumb':result["snippet"]["thumbnails"]["high"]["url"]
            }
            relacionados.append(vr)

        context = {
           'video':video,
           'name':name.replace('"',""),
           'relacionados':relacionados
        }
    else:

        context = {}

    

    if (request.is_ajax()):
        response = {
            "html" : render(request, 'song/inicio.html', context ).getvalue().decode("utf-8") ,
            "success" : True,
            "get": True,
            "url":"/"
        }

        return JsonResponse(response, safe=False) 
    else:       
        return render(request, 'song/inicio.html', context )