from django.db import models
from web.validators import valid_extension, valid_extension_mp3
import os
from django.template import defaultfilters
import datetime
from django.conf import settings



YEAR_CHOICES = []
for r in range(1930, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r,r))


def generate_path(instance, filename):
    folder = "song_" + str(instance.user) 
    return os.path.join("adjuntos", folder, filename)   


class Genre(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, unique=True)
    
    def __str__(self):
        return self.name
     
class Artist(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40)
    photo = models.ImageField(blank=False, null=False, upload_to=settings.URL_MYMEDIA+'artist/photos', validators=[valid_extension], default = 'static/images/default-profile.png')
    
    followers = models.ManyToManyField('web.Usuario', symmetrical = True, blank=True)
    
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if (hasattr(self, "artist")):
            self.name = self.artist.stage_name
            self.photo = self.artist.photo
        
        super(Artist, self).save(*args, **kwargs)

from datetime import date
from django.utils import timezone

class Album(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, blank = False, null=False)
    photo = models.ImageField(blank=False, null=False, upload_to=settings.URL_MYMEDIA+'album/photos', validators=[valid_extension])
    release_date = models.DateField(default=timezone.now(), null = False, blank = False)
    
    slug = models.SlugField(max_length=100, blank = True)
    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = defaultfilters.slugify(self.name)
            super(Album, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name

    class Meta:
        permissions = (
            # Permission identifier     human-readable permission name
            
            ("add_user_album",          "Can user add album"),
            ("change_user_album",       "Can user change album"),
            ("delete_user_album",       "Can user delete album"),
        )
        unique_together = ('name', 'artist',)


from datetime import timedelta
from django.utils import timezone
# Create your models here.
class Song(models.Model):
    file = models.FileField(null = True, blank = True,upload_to=settings.URL_MYMEDIA+'songs/music',validators=[valid_extension_mp3])
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40)
    id_youtube = models.CharField(max_length=11, unique = True)
    link_download = models.CharField(max_length=150, blank = True)
    photo = models.ImageField(blank=False, null=False, upload_to=settings.URL_MYMEDIA+'songs/photos', validators=[valid_extension])
    release_date = models.DateField(default=date.today(), null = False, blank = True)
    lyric =  models.TextField(null = True, blank = True)
    duration = models.DurationField(default=timedelta(), blank=True, null=False)

    genre = models.ForeignKey(Genre, on_delete=models.PROTECT)
    time_stamp = models.DateTimeField(auto_now=True)
    artists = models.ManyToManyField(Artist, blank = True)
    
    album = models.ForeignKey(Album, null = True , blank = True, on_delete=models.SET_NULL)
    
    num_downloads = models.IntegerField(default=0, blank=True)

    downloads = models.ManyToManyField('web.Usuario', symmetrical = True, related_name="downloads")
    
    likes = models.ManyToManyField('web.Usuario', symmetrical = True, blank=True)
    slug = models.SlugField(max_length=100, blank = True, unique = True)

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.name  + ' ' + str(self.id_youtube) )   
        super(Song, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name
    
    class Meta:
        permissions = (
            # Permission identifier     human-readable permission name
            
            ("add_user_song",          "Can user add song"),
            ("change_user_song",       "Can user change song"),
            ("delete_user_song",       "Can user delete song"),
        )     

class Comment_Song (models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField()
    time_stamp = models.DateTimeField(auto_now_add=True, blank=True)
    
    author = models.ForeignKey("web.Usuario", null = False , blank = True, on_delete=models.DO_NOTHING)
    song = models.ForeignKey(Song, null = False , blank = True, on_delete=models.CASCADE)



class PlayList(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40)
    songs = models.ManyToManyField(Song, blank=True)
    user = models.ForeignKey('web.Usuario', blank = True, on_delete=models.CASCADE)
    private = models.BooleanField(blank = True, default=False, null=False)
    
    def __str__(self):
        return self.name
    
class View(models.Model):
    """docstring for Views"""
    country = models.CharField(max_length=40)
    date = models.DateField(default=date.today(), blank=True)
    song = models.ForeignKey(Song, blank=True, null=True, on_delete=models.CASCADE)
    playlist = models.ForeignKey(PlayList, blank=True, null=True, on_delete=models.CASCADE)   
    
    
    
    
    
    
    
    
    
    
