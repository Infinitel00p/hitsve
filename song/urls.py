"""hits URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from .views import *
from django.contrib.auth.decorators import permission_required, login_required



urlpatterns = [
   #Admin 
    url(r'^nueva$', permission_required('song.add_song')(createSongAdmin), name='crear_cancion'),
    url(r'^editar/(?P<id>\d+)$', permission_required('song.change_song')(updateSongAdmin), name='editar_cancion'),
    url(r'^lista$', permission_required('song.change_song')(listSong), name='listar_canciones'),
    #----------album ------------
    url(r'^album/nuevo$', permission_required('song.add_song')(createAlbumAdmin), name='crear_album'),
    url(r'^album/(?P<id>\d+)/cancion$', permission_required('song.add_song')(createAlbumSongAdmin), name='crear_album_song'),
    url(r'^album/editar/(?P<id>\d+)$', permission_required('song.add_song')(updateAlbumAdmin), name='editar_album_song'),
    url(r'^album/lista$', permission_required('song.add_song')(listAlbum), name='listar_albums'),
    url(r'^album/delete/(?P<id>\d+)$', permission_required('song.add_song')(deleteAlbum), name='delete_album'),
    url(r'^ajax/getalbums/$', permission_required('song.add_song')(get_albums), name='get_albums'),

    #userArtist
    url(r'^new$', permission_required('song.add_user_song')(createSongUser), name='new_cancion'),
    url(r'^update/(?P<id>\d+)$', permission_required('song.change_user_song')(updateSongUser), name='update_cancion'),
    url(r'^delete/(?P<id>\d+)$', permission_required('song.change_user_song')(deleteSongUser), name='delete_song_user'),
    url(r'^ajax/getalbumsuser$', permission_required('song.change_user_song')(get_albums_user), name='get_albums_user'),
    url(r'^ajax/likesong$', login_required(like_song), name='like_cancion'),
    #----------album ------------
    url(r'^album/new$', permission_required('song.change_user_song')(createAlbumUser), name='crear_album_user'),
    url(r'^album/(?P<id>\d+)/new$', permission_required('song.change_user_song')(createAlbumSongUser), name='crear_album_song_user'),
    url(r'^album/update/(?P<id>\d+)$', permission_required('song.change_user_song')(updateAlbumUser), name='editar_album_user'),
    url(r'^album/delete/(?P<id>\d+)$', permission_required('song.change_user_song')(deleteAlbumUser), name='delete_album_user'),
    url(r'^album/(?P<ida>\d+)/delete/(?P<ids>\d+)$', permission_required('song.change_user_song')(deleteAlbumSongUser), name='delete_album_song_user'),
    

    url(r'^(?P<slug>[\w-]+)/$', detail, name='detail_cancion'),
    url(r'^download/(?P<id>\d+)$', download, name='download_cancion'),

    url(r'^getlink$', get_link_download, name='get_link'),
    url(r'^sum/(?P<id>\d+)$', sum, name='sum'),


    url(r'^ajax/likeartist$', login_required(follow_artist), name='follow_artist'),

    url(r'^ajax/viewsong$', view_song, name='view_song'),
    url(r'^ajax/viewplaylist$', login_required(view_playlist), name='view_playlist'),


    #----------- playList ----------------
    url(r'^playlist/nueva$', permission_required('song.add_playlist')(createPlayListAjax), name='crear_playlist_ajax'),
    url(r'^playlist/add$', permission_required('song.add_playlist')(addToPlayListAjax), name='add_Toplaylist_ajax'),
    url(r'^playlist/delete/(?P<id>\d+)$', permission_required('song.add_playlist')(deletePlayList), name='delete_playlist_ajax'),
    url(r'^playlist/(?P<idp>\d+)/delete/(?P<ids>\d+)$', permission_required('song.change_user_song')(deletePlayListSong), name='delete_playlist_song_user'),

    url(r'^ajax/getplaylist/$', login_required(getPlayList), name='get_playList'),
    url(r'^ajax/getalbumsongs/$', login_required(getAlbumSongs), name='get_album_songs'),


    
    url(r'^albums/(?P<id>\d+)$', albumList, name='detail_album'),
    url(r'^playlist/(?P<id>\d+)$', playList, name='detail_playlist'),

    #------- comentarios -------

    url(r'^ajax/comentar$', login_required(comment), name='comment_cancion'),
    url(r'^ajax/paginar$', comment_pag, name='comment_paginar'),


    url(r'^denunciar$', delation, name='delation'),

    url(r'^p$', new, name='new'),



    #Para La API
    url(r'^api/getlink$', api_get_link, name='api_get_link'),

]

"""permission_required('song.add_song')"""





