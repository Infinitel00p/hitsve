from django import forms
from django.forms.widgets import Widget
from song import models
from pip._vendor.pkg_resources import require

from django.forms import extras

class AlbumForm (forms.ModelForm):
    
    
    class Meta:
        model = models.Album
        
        field = (
                'name',
                'artist',
                'photo',
                'release_date',
            )
        
        labels = {
                'name': 'Nombre',
                'artist': 'Artista',
                'photo': 'Portada',
                'release_date': 'Fecha de lanzamiento',
    
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            'photo' : forms.FileInput(attrs = {'class':'form-group', 'onchange':"upload_img(this);"}),
            'release_date' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date'}),
            'artist' : forms.Select(attrs = {'class':'select-form'}),
            }
        
        exclude = ['id']

    def clean_artist(self):
        artist = self.cleaned_data.get('artist')
        
        if artist is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return artist

    def clean_release_date(self):
        release_date = self.cleaned_data.get('release_date')
        
        if ( release_date is None):
            raise forms.ValidationError("Este campo es obligatorio")

        return release_date

    def clean_photo(self):
        photo = self.cleaned_data.get('photo')
        
        if photo == "":
            raise forms.ValidationError("Este campo es obligatorio")
    
        return photo




class AlbumUserForm (forms.ModelForm):
    
    
    class Meta:
        model = models.Album
        
        field = (
                'name',
                'photo',
                'release_date',
            )
        
        labels = {
                'name': 'Nombre',
                'photo': 'Portada',
                'release_date': 'Fecha de lanzamiento',
    
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            'photo' : forms.FileInput(attrs = {'class':'form-group', 'onchange':"upload_img(this);"}),
            'release_date' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date'}),
            }
        
        exclude = ['id', 'artist']


    def clean_release_date(self):
        release_date = self.cleaned_data.get('release_date')
        
        if ( release_date is None):
            raise forms.ValidationError("Este campo es obligatorio")

        return release_date

    def clean_photo(self):
        photo = self.cleaned_data.get('photo')
        
        if photo == "":
            raise forms.ValidationError("Este campo es obligatorio")
    
        return photo


    
    
class GenreForm (forms.ModelForm):
    
    
    class Meta:
        model = models.Genre
        
        field = (
                'name',
            )
        
        labels = {
                'name':'Nombre',
    
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            }
        
        exclude = ['id']
        
        
    def clean_name(self):
        name = self.cleaned_data.get('name')

        if models.Genre.objects.filter(name=name).exists():
            raise forms.ValidationError("Este nombre de género ya esta en uso.")

        return name

TRUE_FALSE_CHOICE = (
    (True, "Privado"),
    (False, "Público")
)

class PlayListForm (forms.ModelForm):
    
    class Meta:
        model = models.PlayList
        
        field = (
                'name',
                'private',
            )
        
        labels = {
                'name':'Nombre',
                'private': 'Privacidad',
    
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            'private' : forms.Select(choices = TRUE_FALSE_CHOICE, attrs = {'class':'select-form'})
            }
        
        exclude = ['id', 'songs', 'user']
        
        
    def clean_name(self):
        name = self.cleaned_data.get('name')
        
        if ( name is None):
            raise forms.ValidationError("Este campo es obligatorio")

        return name
    
    
class ArtistForm (forms.ModelForm):
    
    
    class Meta:
        model = models.Artist
        
        field = (
                'name',
                'photo',
            )
        
        labels = {
                'name':'Nombre',

                'photo':'Foto',
    
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),

            'photo':forms.FileInput(attrs = {'class':'form-group', 'onchange':"upload_img(this);"}),
            }
        
        exclude = ['id']
        
        
from ckeditor.widgets import CKEditorWidget
from ckeditor.fields import RichTextFormField
from smart_selects.form_fields import ChainedSelect, ChainedSelectMultiple


ALBUM_CHOICES = (
    ('0', 'form'),
)

class SongAdminForm (forms.ModelForm):
    album = forms.CharField(widget=forms.Select(choices= ALBUM_CHOICES,attrs={'class':'select-form'}), required=False)
    
    class Meta:
        model = models.Song
        
        field = (
                'name',
                'id_youtube',
                'lyric',
                'photo',
                'release_date',
                'artists',
                'genre',
            )
        
        labels = {
                'name':'Título',
                'id_youtube':'Youtube Id',
                'lyric':'Letra',
                'photo':'Portada',
                'release_date':'Fecha de lanzamiento',
                'artists':'Cantantes',
                'genre':'Género',
                'album':'Album',
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            'id_youtube' : forms.TextInput(attrs = {'class':'form-group'}),
            'lyric' : CKEditorWidget(),
            'photo' : forms.FileInput(attrs = {'class':'form-group', 'onchange':"upload_img(this);"}),
            'release_date' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date'}),
            'artists' : forms.SelectMultiple(attrs = {'class':'select-form'}),
            'genre' : forms.Select(attrs = {'class':'select-form'}),
            }
        
        exclude = ['num_downloads', 'duration', 'id','likes','slug','file', 'album', 'downloads', 'link_download']
        
    def clean_genre(self):
        genre = self.cleaned_data.get('genre')
        
        if genre is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return genre

    def clean_artists(self):
        artists = self.cleaned_data.get('artists')
        c = artists.all().count()  

        if c == 0:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return artists

    def clean_release_date(self):
        date = self.cleaned_data.get('release_date')
        
        if ( date is None):
            raise forms.ValidationError("Este campo es obligatorio")

        return date


class AlbumSongAdminForm (forms.ModelForm):
    
    class Meta:
        model = models.Song
        
        field = (
                'name',
                'id_youtube',
                'lyric',
                'artists',
                'genre',
            )
        
        labels = {
                'name':'Título',
                'id_youtube':'Youtube Id',
                'lyric':'Letra',
                'artists':'Cantantes',
                'genre':'Género',
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            'id_youtube' : forms.TextInput(attrs = {'class':'form-group'}),
            'lyric' : CKEditorWidget(),
            'artists' : forms.SelectMultiple(attrs = {'class':'select-form'}),
            'genre' : forms.Select(attrs = {'class':'select-form'}),
            }
        
        exclude = ['num_downloads', 'duration', 'id','likes','slug','file','photo','release_date', 'album', 'downloads', 'link_download']
        
    def clean_genre(self):
        genre = self.cleaned_data.get('genre')
        
        if genre is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return genre

    def clean_artists(self):
        artists = self.cleaned_data.get('artists')
        c = artists.all().count()  

        if c == 0:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return artists




        
class SongArtistForm (forms.ModelForm):
    album = forms.CharField(widget=forms.Select(choices= ALBUM_CHOICES), required=False)
    
    class Meta:
        model = models.Song
        
        field = (
                'name',
                'id_youtube',
                'link_download',
                'lyric',
                'photo',
                'release_date',
                'genre',
            )
        
        labels = {
                'name':'Título',
                'id_youtube':'Youtube Id',
                'lyric':'Letra',
                'photo':'Portada',
                'release_date':'Fecha de lanzamiento',
                'genre':'Género',
                'file':'Subir',
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            'id_youtube' : forms.TextInput(attrs = {'class':'form-group'}),
            'lyric' : CKEditorWidget(),
            'photo' : forms.FileInput(attrs = {'class':'form-group', 'onchange':"upload_img(this);"}),
            'release_date' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date'}),
            'genre' : forms.Select(attrs = {'class':'select-form'}),
            'file': forms.FileInput(attrs = {'class':'form-group', 'onchange':""}),
            }
        
        exclude = ['num_downloads','duration', 'id','likes','slug', 'album', 'artists', 'link_download',  'downloads']
        
    def clean_file(self):
        file = self.cleaned_data.get('file')
        
        if file is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return file

    def clean_genre(self):
        genre = self.cleaned_data.get('genre')
        
        if genre is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return genre



class AlbumSongArtistForm (forms.ModelForm):
    class Meta:
        model = models.Song
        
        field = (
                'name',
                'id_youtube',
                'link_download',
                'lyric',
                'genre',
            )
        
        labels = {
                'name':'Título',
                'id_youtube':'Youtube Id',
                'lyric':'Letra',
                'genre':'Género',
                'file':'Subir',
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form-group'}),
            'id_youtube' : forms.TextInput(attrs = {'class':'form-group'}),
            'lyric' : CKEditorWidget(),
            'genre' : forms.Select(attrs = {'class':'select-form'}),
            'file': forms.FileInput(attrs = {'class':'form-group', 'onchange':""}),
            }
        
        exclude = ['num_downloads', 'duration', 'id','likes','slug', 'album','photo', 'artists', 'link_download', 'release_date',  'downloads']
        
    def clean_file(self):
        file = self.cleaned_data.get('file')
        
        if file is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return file
        
        
        
        