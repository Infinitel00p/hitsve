from django.contrib import admin

# Register your models here.

from .models import *

class AdminGenre(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ["name"]
    class Meta:
        model = Genre
    
admin.site.register(Genre, AdminGenre)

class AdminArtist(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ["name"]
    class Meta:
        model = Artist
    
admin.site.register(Artist, AdminArtist)

class AdminAlbum(admin.ModelAdmin):
    list_display = ['name', 'slug']
    search_fields = ["name"]
    class Meta:
        model = Album
    
admin.site.register(Album, AdminAlbum)

class AdminSong(admin.ModelAdmin):
    list_display = ['name','slug','release_date','id_youtube', 'photo', 'file']
    list_filter = ["release_date", 'genre']
    search_fields = ["name", "artist", 'genre', 'album']
    class Meta:
        model = Song
    
admin.site.register(Song, AdminSong)


class AdminPlayList(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ["name"]
    class Meta:
        model = PlayList
    
admin.site.register(PlayList, AdminPlayList)