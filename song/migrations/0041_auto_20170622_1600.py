# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-22 20:00
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('song', '0040_auto_20170619_1109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='album',
            name='release_date',
            field=models.DateField(default=datetime.date(2017, 6, 22)),
        ),
        migrations.AlterField(
            model_name='song',
            name='release_date',
            field=models.DateField(blank=True, default=datetime.date(2017, 6, 22)),
        ),
    ]
