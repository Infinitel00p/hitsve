# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-11 04:20
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('song', '0048_auto_20170707_0259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='album',
            name='release_date',
            field=models.DateField(default=datetime.date(2017, 7, 11)),
        ),
        migrations.AlterField(
            model_name='song',
            name='release_date',
            field=models.DateField(blank=True, default=datetime.date(2017, 7, 11)),
        ),
        migrations.AlterField(
            model_name='view',
            name='date',
            field=models.DateField(blank=True, default=datetime.date(2017, 7, 11)),
        ),
    ]
