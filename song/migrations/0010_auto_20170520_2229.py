# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-21 02:29
from __future__ import unicode_literals

from django.db import migrations
import django.db.models.deletion
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('song', '0009_auto_20170520_2224'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='song',
            name='album',
        ),
        migrations.AddField(
            model_name='song',
            name='album',
            field=smart_selects.db_fields.ChainedForeignKey(blank=True, chained_field='artists', chained_model_field='artist', null=True, on_delete=django.db.models.deletion.SET_NULL, to='song.Album'),
        ),
    ]
