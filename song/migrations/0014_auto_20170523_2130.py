# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-24 01:30
from __future__ import unicode_literals

from django.db import migrations, models
import web.validators


class Migration(migrations.Migration):

    dependencies = [
        ('song', '0013_auto_20170521_1341'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='song',
            options={'permissions': (('add_user_song', 'Can user add song'), ('change_user_song', 'Can user change song'), ('delete_user_song', 'Can user delete song'))},
        ),
        migrations.AlterField(
            model_name='song',
            name='file',
            field=models.FileField(blank=True, null=True, upload_to='media/songs/music', validators=[web.validators.valid_extension_mp3]),
        ),
    ]
