# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-09 01:01
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('song', '0020_auto_20170608_2100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='release_date',
            field=models.DateField(blank=True, default=datetime.datetime(2017, 6, 9, 1, 1, 29, 986516, tzinfo=utc)),
        ),
    ]
