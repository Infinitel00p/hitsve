
        (function () {

          $.urlParam = function(name,link){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(link)//window.location.href
            if (results == null)
                return 1;
            return results[1] || 0;
          }

        $("a.link-pag").on("click", function (e) {
              e.preventDefault();

              link = $(this).prop('href');
              
              var page = parseInt($.urlParam('page', link));

              $.ajax({
                type: 'GET',
                url: "{% url 'noticias:comment_paginar' %}",
                data: {'id' : "{{post_id}}", 'page':page},
                dataType: 'html',
                success: function (data) {

                  var stateObj = { foo: "page" };
                  history.pushState(stateObj, "page "+page, "?page="+page);
                  $(".div-comment").html(data);
                    
                },
                error : function(xhr,errmsg,err) {
                    $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                        " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                    alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                }
            });
        });

        }).call(this);
