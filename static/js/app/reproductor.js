//--------------------  repro -------------------------


// body...
var audio = $('audio');
var timeout;

function moveSliderX(e) {
    var pos = $(".rep-body .reproductor .reproduction").offset(),
    posX = e.pageX - pos.left;
    if(posX >= 0 && posX <= $(".rep-body .reproductor .reproduction").outerWidth()){
        try {
            var duration = audio.get(0).duration;
        }
        catch(err) {
            var duration = 0;
        }
        var barraActual = posX;
        var longitudBarra = parseInt($(".rep-body .reproductor .reproduction").css("width").split("px")[0]);
        var currentTime = ((barraActual * duration)/longitudBarra);

       
        try {
             audio.get(0).currentTime = currentTime;
        }
        catch(err) {
        }

        $(".rep-body .reproductor .reproduction .reproducted").css("width", posX + "px");
        $(".rep-body .reproductor .reproduction .circle").css("left", posX + "px");
    }
}


function moveSliderVolX(e) {
    var pos = $(".volumen .volumen-body .reproduction").offset(),
        posX = e.pageX - pos.left;

    
    if(posX >= 0 && posX <= $(".volumen .volumen-body  .reproduction").outerWidth()){
        var volMax = 100;
        var barraActual = posX;
        var longitudBarra = parseInt($(".volumen .volumen-body .reproduction").css("width").split("px")[0]);
        var volume = ((barraActual * volMax)/longitudBarra);
        

        if(volume > 4 && volume <= 50 ){
            $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-up ").show();
            $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-down ").hide();
            
            try {
                audio.get(0).volume = volume/100;
            }
            catch(err) {
            }
        }else if (volume > 50 ){
            $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-up ").show();
            $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-down ").hide();
            
            try {
                audio.get(0).volume = volume/100;
            }
            catch(err) {
            }
        }else {
            $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-down ").show();
            $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-up ").hide()
            
            try {
                audio.get(0).volume = 0;
            }
            catch(err) {
            }
        }



        $(".volumen .volumen-body .reproduction .reproducted").css("width", posX + "px");
        $(".volumen .volumen-body .reproduction .circle").css("left", posX + "px");
    }
}

function moveCircleTimeX(e) {
    var pos = $(".rep-body .reproductor .reproduction").offset(),
    posX = e.pageX - pos.left;
    if(posX >= 0 && posX <= $(".rep-body .reproductor .reproduction").outerWidth()){
        try {
            var duration = audio.get(0).duration;
        }
        catch(err) {
            var duration = 0;
        }

        var barraActual = posX;
        var longitudBarra = parseInt($(".rep-body .reproductor .reproduction").css("width").split("px")[0]);
        var currentTime = (barraActual * duration)/longitudBarra


        $(".rep-body .reproductor .reproduction .circle-time").css("left", posX + "px");
        $(".rep-body .reproductor .reproduction .circle-time").text(segundosMinutos(currentTime));
    }
}


function song(action) {

    if (!$(audio).attr("src") ){

        var song = $(".song:first-child");
        if ($(song).attr("data-id") ){
            changeSong(song);
        }
    }else
    if (action == "play") {
        if ( $('audio').get(0).readyState == 4)
            $('audio').get(0).play();
    } else if (action == "pause") {
        audio.get(0).pause();
    } else if (action == "stop") {
        audio.get(0).load();
    }
}

function segundosMinutos(duration) {
    var segundos = duration,
        minutos = Math.floor(segundos/60),
        seg = Math.round(segundos % 60);

    if (minutos < 10){
        minutos = "0" + minutos;
    }

    if (seg < 10) {
        seg = "0"+seg;
    }

    if (seg == 60){
        minutos = parseInt(minutos) + 1;
        minutos = "0"+ minutos;
        seg = "00";
    }

    duracionTotal = minutos + ":" + seg;
    return duracionTotal;
}

function changeTime(duration, actual, width) {
    var currentWidth = (actual * width)/duration;

    $(".rep-body .reproductor .reproduction .reproducted").css("width", currentWidth + "px");
    $(".rep-body .reproductor .reproduction .circle").css("left", currentWidth + "px");

    $(".rep-body .reproductor .reproduction .time .played").text(segundosMinutos(actual));
}


function changeSong(song) {
    $(".rep-body .reproductor .reproduction .time .duration").text("00:00");
    $('.load-player').show();
    var name = $(song).attr("data-name");
        // conseguir la url
        if (!$(song).attr("data-url") && $(song).attr("class") ){
            var id = $(song).attr("data-id");
           
            $.ajax({
                type: 'GET',
                url: "/player/getlink",
                data: {'id' : parseInt(id)},
                dataType: 'json',
                success: function (data) {
                    $(song).attr("data-url", data);
                    play_song(data, name, $(song));
                    
                    
                },
                error : function(xhr,errmsg,err) {
                    $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                        " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                    console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                   
                }
            });

            prefetch(song);

        }else{
            play_song($(song).attr("data-url"), name, song);
        }

        $("#reproductor .titulo .nombre").text(name);
        $("#reproductor .img-cover > .img-back").attr("src","/"+$(song).attr("data-photo"));
        

}

function prefetch(song){
    next = $(song).next();

    if (!$(next).attr("data-url") && $(next).attr("class") ){
        var id = $(next).attr("data-id");
        $.ajax({
            type: 'GET',
            url: "/player/getlink",
            data: {'id' : parseInt(id)},
            dataType: 'json',
            success: function (data) {
                $(next).attr("data-url", data);

                
            },
            error : function(xhr,errmsg,err) {
                $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                    " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });  
    }
}

function play_song(url, name, song) {
    $('audio').attr("src", url);
    $('audio').attr("data-name", name);

    $('.song.playing').children('.status').remove();
    $('.song.playing').removeClass('playing');
    $(song).addClass('playing');        
    //$(song).append('<i class="icon-play status"></i>');
    $(song).append('<div id="sound-on" class="music status">'+
                      '<div id="bar1"></div>'+
                      '<div id="bar2"></div>'+
                      '<div id="bar3"></div>'+
                    '</div>');

    
    $(".music.status").hide();

    try {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            timeout = setTimeout(sum_play,30000,parseInt($(song).attr("data-id")));
        }else{
            var playPromise = $('audio').get(0).play(); 
        }
        
    }
    catch(err) {
        var playPromise = undefined;
    }
    

    if (playPromise !== undefined) {
      playPromise.then(function() {
        timeout = setTimeout(sum_play,20000,parseInt($(song).attr("data-id")));
      }).catch(function(error) {
        $(song).removeAttr("data-url","");
        var n = $(song).next();
        n.removeAttr("data-url","");
        changeSong($(song));
      });
    }
}



var csrftoken = getCookie('csrftoken');
function sum_play(id) {
    $.ajax({
        type: 'POST',
        url: "/player/ajax/viewsong",
        data: {'id' : id, csrfmiddlewaretoken : csrftoken},
        dataType: 'json',
        success: function (data) {

        },
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });  
}


//For getting CSRF token
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function prevNextSong(type) {
    var currentSong = $(".song.playing"), song;

    if (type == "prev") {
        song = $(currentSong).prev();
    } else {
        song = $(currentSong).next();
    }

    if ($(song).attr("class")){
        clearTimeout(timeout);
        changeSong(song);
    }
}

//-->funciones

//eventos


//eventos de audio
$('audio').on("play", function () {
    $(".control .buttons-time .buttons .icon-play").hide();
    $(".control .buttons-time .buttons .icon-pause").show();
});
$('audio').on("pause", function () {
    $(".control .buttons-time .buttons .icon-pause").hide();
    $(".control .buttons-time .buttons .icon-play").show();
});

//Este es para el stop
$('audio').on("loadeddata", function () {
    $(".rep-body .reproductor .reproduction .time .duration").text(segundosMinutos(audio.get(0).duration));
    changeTime(this.duration, 0, 0);

    var n = $( "#reproductor .list .lista-back .song"  ).length;
    var bottom =  (($(".playing").height()+20) * (n - parseInt($(".playing").attr("data-count"))) ) -15 -10 ;
    $('.status #bar1').css("bottom",bottom+"px");
    $('.status #bar2').css("bottom",bottom+"px");
    $('.status #bar3').css("bottom",bottom+"px");

    // ----------------------------------------------------------------------------------------------------------------------------
    $('.load-player').hide();

    $(".control .buttons-time .buttons .icon-play").trigger("click");
});

$('audio').on("timeupdate", function () {
    changeTime(this.duration, this.currentTime, $(".rep-body .reproductor .reproduction").width());
});

$('audio').on("playing", function () {
    
    $(".music.status").show();
});

$('audio').on("ended", function () {
    prevNextSong("next");
});

$('audio').on('progress', function() {
    var duration =  this.duration;
    if (duration > 0) {
        
      for (var i = 0; i < this.buffered.length; i++) {
            if (this.buffered.start(this.buffered.length - 1 - i) < this.currentTime) {
                $(".rep-body .reproduction .buffered").css("width", (this.buffered.end(this.buffered.length - 1 - i) / duration) * 100 + "%") ;
                break;
            }
        }
    }
  });
//--> eventos del audio




//eventos del DOM
$("#reproductor .rep-body .reproductor .reproduction").on("mousedown", function (e) {
    
    e.preventDefault();

    moveSliderX(e);

    $("html").on("mousemove", function (e) {
        moveSliderX(e);
    }).on("mouseup", function (e) {
        //$("")
        $(this).off("mousemove");
    });
});


$("#reproductor .volumen .volumen-body .reproduction").on("mousedown", function (e) {
    
    e.preventDefault();

    moveSliderVolX(e);

    $("html").on("mousemove", function (e) {
        moveSliderVolX(e);
    }).on("mouseup", function (e) {
        //$("")
        $(this).off("mousemove");
    });


});


$("#reproductor .rep-body .reproductor .reproduction").on("mousemove", function (e) {
    e.preventDefault();

    $('.circle-time').show();

    moveCircleTimeX(e);
}).on("mouseout", function (e) {
    $('.circle-time').hide();
});



$(".control .buttons-time .buttons .icon-play").click(function () {
    song("play");
    $(".music.status").show();
});

$(".control .buttons-time .buttons .icon-pause").click(function () {
    song("pause");

    $(".music.status").hide();
});

$(".control .buttons-time .buttons .icon-stop").click(function () {
    song("stop");
});

$(".control .buttons-time .buttons .icon-to-start").click(function () {
    prevNextSong("prev");

    $(".music.status").hide();
});
$(".control .buttons-time .buttons .icon-to-end-alt").click(function () {
    prevNextSong("next");

    $(".music.status").hide();
});

$(".control .buttons-time .buttons .volumen .icon-vol").click(function (e) {

        var pos = $(".volumen .volumen-body .reproduction").offset();

    try{
    if (audio.get(0).volume == 0){

        var posX = 69.7 ;
        $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-down ").hide();
        $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-up ").show();

        audio.get(0).volume = 1;

        
    }else{
        var posX = e.pageX - pos.left;
        $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-down ").show();
        $(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-up ").hide();
        audio.get(0).volume = 0;
    }
    } catch(e){}


        $(".volumen .volumen-body .reproduction .reproducted").css("width", posX + "px");
        $(".volumen .volumen-body .reproduction .circle").css("left", posX + "px");
});



$(".control .buttons-time .buttons .icon-play").show();
$(".control .buttons-time .buttons .icon-pause").hide();
$(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-down ").hide();
$(".control .buttons-time .buttons .volumen .icon-vol .icon-volume-up ").show();
$(".music.status").hide();

//barras de animacion


//desplegar lista
$(".rep-body .list-img .icon-list").click(function () {
    var h = $('.rep-body .list-img .icon-list-checkbox').is(':checked');
    if (h){
        $(".rep-body .list-img .icon-list-checkbox").prop('checked', false);
    }else {
        $(".rep-body .list-img .icon-list-checkbox").prop('checked', true);
    }
});

   $(".lista .lista-back").on("click", ".song",function () {
        clearTimeout(timeout);
        changeSong($(this));
    });



    $(document).on('click', '.only-play' ,function () {

        var name = $(this).attr("data-name"),
            id = $(this).attr("data-id"),
            photo = $(this).attr("data-photo");

        $("#reproductor .list .lista-back").html(
            '<div class="song playing" data-name="'+name+'" data-id="'+id+'" data-photo="'+photo+'" data-count="0">'+
                '<span class=""> '+name+' </span>'+

            '</div>'
        );

        changeSong($('.song.playing'));

        $("#reproductor .list .lista .title-total > .total .number").text("1");
        $('#repro').toggle('slow');
   });

   $(document).on("click",".add-to-play",function () {
        var name = $(this).attr("data-name"),
            id = $(this).attr("data-id"),
            photo = $(this).attr("data-photo");

         var n = $( "#reproductor .list .lista-back .song"  ).length;
        $("#reproductor .list .lista-back").append(
            '<div class="song new" data-name="'+name+'" data-photo="'+photo+'" data-id="'+id+'" data-count="'+n+'">'+
                '<span class=""> '+name+' </span>'+

            '</div>'
        );

        $("#reproductor .list .lista .title-total > .total .number").text(n+1);
   });


   $(document).on("click",".to-play-list",function () {
    
        var name = $(this).attr("data-name"),
            id = $(this).attr("data-id");

        if ($(this).attr("data-url") == "album"){
            url = "/player/ajax/getalbumsongs/";
        }else if($(this).attr("data-url") == "playlist"){
            url = "/player/ajax/getplaylist/";
        }


        $.ajax({
            type: 'GET',
            url: url,
            data: {'id' : parseInt(id)},
            dataType: 'json',
            success: function (data) {
                $("#reproductor .list .lista-back").text("")
                for (var i = 0; i < data.length; i++) {

                    $("#reproductor .list .lista-back").append(
                        '<div class="song" data-name="'+data[i].name+'" data-photo="'+data[i].photo+'" data-id="'+data[i].id+'" data-count="'+i+'">'+
                            '<span class=""> '+data[i].name+' </span>'+

                        '</div>'
                    ); 
                }

                $(audio).attr("src","");
                $("#reproductor .list .lista .title-total > .tile").text(name);
                $("#reproductor .list .lista .title-total > .total .number").text(data.length);

                song("play");
                
                
            },
            error : function(xhr,errmsg,err) {
                 // add the error to the dom
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    });


    $(document).on("click", ".ctr-down",function () {
        
        var url = $(".playing.song").attr("data-url");
        $(this).attr("href",""+url);
    });