from web.models import Usuario
def global_data(request):
	
	try:
		user = Usuario.objects.get(username=request.user.username)
		context={'user_log' : user}
		return context		
	except:
		return {'user_log':None}
    
    