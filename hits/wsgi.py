"""
WSGI config for hits project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os, sys
sys.path.append('C:/Apache24/htdocs/hitsweb/hits/hits')
sys.path.append('C:/Apache24/htdocs/hitsweb/hits')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hits.settings")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
