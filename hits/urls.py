"""hits URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete
from ckeditor_uploader import views
from django.contrib.auth.decorators import permission_required
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^usuarios/', include('web.urls', namespace="usuarios")),
    url(r'^noticias/', include('post.urls', namespace="noticias")),
    url(r'^', include('song.urls', namespace="player")),
    url(r'^', include('principal.urls',namespace="principal") ),
    
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    
    url(r'^upload/', permission_required('post.change_post')(views.upload), name='ckeditor_upload'),
    url(r'^browse/', permission_required('post.change_post')(views.browse), name='ckeditor_browse'),


    #reset password
    url(r'^reset/pasword_reset$', password_reset, {'template_name':'password_reset/password_reset_form.html','email_template_name':'password_reset/password_reset_email.html'}, name='password_reset'),
    url(r'^reset/pasword_reset_done$', password_reset_done, {'template_name':'password_reset/password_reset_done.html'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)$', password_reset_confirm, {'template_name':'password_reset/password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^reset/done$', password_reset_complete, {'template_name':'password_reset/password_reset_complete.html'}, name='password_reset_complete'),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)