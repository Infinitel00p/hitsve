
from django import forms
from .models import *
from django.forms.widgets import Widget

from django.forms import extras

class UsuarioForm (forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput, label="Contraseña")
    password2 = forms.CharField(widget=forms.PasswordInput, label="Confirmar contraseña")
    
    class Meta:
        model = Usuario
        
        field = (
                'username',
                'email',
                'country',
                'birthdate',
            )
        
        labels = {
                'username':'Nombre de Usuario',
                'email' : 'Correo',
                'country': 'País',
                'birthdate': 'Fecha de nacimiento',
    
            }
    
        widgets = {
            'username' : forms.TextInput(attrs = {'class':'form-control'}),
            'email' : forms.EmailInput(attrs = {'class':'form-control'}),
            'country' : forms.Select(attrs = {'class':'select-form'}),
            'birthdate' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date'}),
            }
        
        exclude = ['stage_name', 'id','last_login','is_superuser','first_name','last_name','is_staff',
                    'is_active','date_joined','follows','front', 'photo', 'groups', 'user_permissions', 
                    'password', 'descrpition', 'website', 'instagram', 'name']

    def clean(self):
        cleaned_data = super(UsuarioForm, self).clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")

        if 8 > len(password1.__str__()):
            self.add_error('password1',"La contraseña debe tener al menos 8 caracteres")
            
        if password1 != password2 :
            self.add_error('password1',"Las contraseña no coinciden")
            #raise forms.ValidationError("Las contraseñas no coinciden", code="password1",)
        return self.cleaned_data

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Este Email ya esta en uso.")

        return email


    def clean_country(self):
        country = self.cleaned_data.get('country')
        if country == "":
            raise forms.ValidationError("Este campo es obligatorio.")
    
        return country


class UsuarioCompleteForm (forms.ModelForm):
    
    class Meta:
        model = Usuario
        
        field = (
                'country',
                'birthdate', 
                'descrpition', 
                'website', 
                'instagram', 
                'name',
                'photo',
                'front', 
                'username', 
                'email',

            )
        
        labels = {
                'username':'Nombre de usuario', 
                'email': 'Correo',
                'country': 'País',
                'birthdate': 'Fecha de nacimiento', 
                'descrpition' : 'Biografía',
                'website' : 'Sitio web',
                'instagram': 'Instagram',
                'photo': 'Foto de perfil',
                'front' : 'Foto de portada',
    
            }
    
        widgets = {
            'username' : forms.TextInput(attrs = {'class':'form-control'}),
            'email' : forms.EmailInput(attrs = {'class':'form-control'}),
            'country' : forms.Select(attrs = {'class':'select-form form-control'}),
            'birthdate' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date form-control'}), 
            'descrpition' : forms.TextInput(attrs = {'class':'form-control'}), 
            'website': forms.URLInput(attrs = {'class':'form-control'}), 
            'instagram':forms.TextInput(attrs = {'class':'form-control'}), 
            'name':forms.TextInput(attrs = {'class':'form-control'}),
            'photo': forms.FileInput(attrs = {'class':'form-control clases'}),
            'front': forms.FileInput(attrs = {'class':'form-control clases'}),
        }
        
        exclude = ['stage_name', 'id','last_login','is_superuser','first_name','last_name','is_staff',
                    'is_active','date_joined','follows','groups', 'user_permissions', 
                    'password']
    """    
    def clean_username(self):
        username = self.cleaned_data.get('username')

        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("Este nombre de usuario ya esta en uso.")

        return username
    """
    """
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(UsuarioCompleteForm, self).__init__(*args, **kwargs)
    """
    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists() and email != self.data['email']:
            raise forms.ValidationError("Este Email ya esta en uso.")

        return email

    def clean_instagram(self):
        instagram = self.cleaned_data.get('instagram')

        if (instagram is not None):
            if instagram[0] != "@":
                raise forms.ValidationError("Ingresa un usuario de instagram valido.")

        return instagram

    def clean_country(self):
        country = self.cleaned_data.get('country')
        
        if country == "" or country is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return country
    
    
class ArtistForm(UsuarioForm):
    
    
    class Meta:
        model = UserArtist
        
        field = [
                'username',
                'email',
                'country',
                'birthdate',
                'stage_name',
            ]
        
        labels = {
                'username':'Nombre de Usuario',
                'stage_name':'Nombre Artistico',
                'email' : 'Correo',
                'country': 'Nacionalidad',
                'birthdate': 'Fecha de nacimiento','stage_name': 'Nombre Artístico',
    
            }
    
        widgets = {
                'username' : forms.TextInput(attrs = {'class':'form-control'}),
                'stage_name' : forms.TextInput(attrs = {'class':'form-control'}),
                'email' : forms.EmailInput(attrs = {'class':'form-control'}),
                'country' : forms.Select(attrs = {'class':'select-form'}),
                'birthdate' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date'}),
            }
        
        exclude = ['singer','id','last_login','is_superuser','first_name','last_name','is_staff',
                    'is_active','date_joined','follows','front','photo', 'groups', 'user_permissions', 
                    'password', 'descrpition', 'website', 'instagram', 'name']
    
    def clean_stage_name(self):
        stage_name = self.cleaned_data.get('stage_name')
        
        if stage_name == "":
            raise forms.ValidationError("Este campo es obligatorio")
    
        return stage_name

    def clean_country(self):
        country = self.cleaned_data.get('country')
        
        if country == "":
            raise forms.ValidationError("Este campo es obligatorio")
    
        return country




class ArtistCompleteForm(UsuarioCompleteForm):
    
    
    class Meta:
        model = UserArtist
        
        field = [
                'country',
                'birthdate',
                'stage_name',
                'descrpition', 
                'website', 
                'instagram', 
                'name',
                'username', 
                'email',
            ]
        
        labels = {
                'username':'Nombre de usuario', 
                'email': 'Correo',
                'stage_name':'Nombre Artistico',
                'country': 'Nacionalidad',
                'birthdate': 'Fecha de nacimiento','stage_name': 'Nombre Artístico',
                'descrpition' : 'Biografía',
                'website' : 'Sitio web',
                'instagram': 'Instagram',
                'photo': 'Foto de perfil',
                'front' : 'Foto de portada',
    
            }
    
        widgets = {
                'username' : forms.TextInput(attrs = {'class':'form-control'}),
                'email' : forms.EmailInput(attrs = {'class':'form-control'}),
                'stage_name' : forms.TextInput(attrs = {'class':'form-control'}),
                'country' : forms.Select(attrs = {'class':'form-control'}),
                'birthdate' : extras.SelectDateWidget(years=range(1900, 2050),attrs = {'class':'select-form-date form-control'}), 
                'descrpition' : forms.TextInput(attrs = {'class':'form-control'}), 
                'website': forms.URLInput(attrs = {'class':'form-control'}), 
                'instagram':forms.TextInput(attrs = {'class':'form-control'}), 
                'name':forms.TextInput(attrs = {'class':'form-control'}),
                'photo': forms.FileInput(attrs = {'class':'form-control clases'}),
                'front': forms.FileInput(attrs = {'class':'form-control clases'}),
            }
        
        exclude = ['singer','id','last_login','is_superuser','first_name','last_name','is_staff',
                    'is_active','date_joined','groups', 'user_permissions', 
                    'password']
    
    """
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ArtistCompleteForm, self).__init__(*args, **kwargs)
    """

    def clean_stage_name(self):
        stage_name = self.cleaned_data.get('stage_name')
        
        if stage_name == "":
            raise forms.ValidationError("Este campo es obligatorio")
    
        return stage_name

    def clean_country(self):
        country = self.cleaned_data.get('country')
        
        if country == "":
            raise forms.ValidationError("Este campo es obligatorio")
    
        return country

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if User.objects.filter(email=email).exists() and email != self.data['email']:
            raise forms.ValidationError("Este Email ya esta en uso.")

        return email

    def clean_instagram(self):
        instagram = self.cleaned_data.get('instagram')

        if (instagram is not None):
            if instagram[0] != "@":
                raise forms.ValidationError("Ingresa un usuario de instagram valido.")

        return instagram



    
    
