
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest,JsonResponse 

# Create your views here.
from .forms import *
from .models import *
from django.views.generic.edit import CreateView
from song.models import Artist, Album, PlayList
# Create your views here.

def register(request):
    return render(request, "usuarios/selection.html", {})


def log_in(request):
    if request.user.is_authenticated():
        return redirect('principal:inicio')


    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        
        user = authenticate(username=username, password=password)

        if user is not None:

            login(request, user)

            next=request.POST.get('next')

            if str(next) in ["None", '']:
                return redirect('principal:inicio')
            else:
                return redirect(next)
        else:
            next=request.POST.get('next')
            if str(next) in ["None", '']:
                action = "/usuarios/login"
            else:
                action = "/usuarios/login?next={0}".format(next)

            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'usuarios/login.html', {'mensaje': 'El Nombre de usuario y la Contraseña no coinciden','next':next, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'usuarios/login.html', {'mensaje': 'El Nombre de usuario y la Contraseña no coinciden','next':next, 'action':action})

    else:
        next = request.GET.get('next')
        if str(next) in ["None", '']:
            action = "/usuarios/login"
        else:
            action = "/usuarios/login?next={0}".format(next)

        if (request.is_ajax()):
            response = {
                "html" : render(request, 'usuarios/login.html', {'next':next, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'usuarios/login.html', {'next':next, 'action':action})
 
 
def sign_up(request):
    if request.user.is_authenticated():
        return redirect('principal:inicio')

    form = UsuarioForm(request.POST or None)
    form.fields['country'].required = False

    action = "/usuarios/register/usuario"

    if request.method == 'POST':
        # print dir(form)  #muestra todos lo que se puede hacer con form
        if form.is_valid():
            
            form_data = form.cleaned_data
            
            user = Usuario()
            user.username=form_data.get("username")
            user.email=form_data.get("email")
            user.set_password(form_data.get("password1"))
            user.country =form_data.get("country")
            user.birthdate =form_data.get("birthdate")
            user.is_active = True
            user.save()
            
            g1 = Group.objects.get(name='usuario')
            user.groups.set([g1,])
            
            login(request, user)
            
            return redirect('principal:inicio')
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'usuarios/register.html', {'form': form, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'usuarios/register.html', {'form': form, 'action':action})
    else:
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'usuarios/register.html', {'form': form, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'usuarios/register.html', {'form': form, 'action':action}) 

from django.contrib.auth.models import Group

def sign_up_artist(request):
    if request.user.is_authenticated():
        return redirect('principal:inicio')

    form = ArtistForm(request.POST or None)
    form.fields['country'].required = False
    form.fields['stage_name'].required = False
     

    action = "/usuarios/register/musico"

    if request.method == 'POST':
        # print dir(form)  #muestra todos lo que se puede hacer con form
        
        name = request.POST.get("stage_name")
        if form.is_valid():
            
            form_data = form.cleaned_data
            
            user = UserArtist()
            user.username=form_data.get("username")
            user.stage_name = name
            user.email=form_data.get("email")
            user.set_password(form_data.get("password1"))
            user.country =form_data.get("country")
            user.birthdate =form_data.get("birthdate")
            user.is_active = True

            user.save()
            g1 = Group.objects.get(name='usuario')
            g2 = Group.objects.get(name='artist')
            user.groups.set([g1,g2,])

            
            login(request, user)
            
            return redirect('principal:inicio')
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'usuarios/register_musician.html', {'form': form, 'stage_name': name, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, 'usuarios/register_musician.html', {'form': form, 'stage_name': name, 'action':action})
    
    else: 
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'usuarios/register_musician.html', {'form': form, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'usuarios/register_musician.html', {'form': form, 'action':action}) 

   
from django.contrib.auth import logout

def log_out(request):
    logout(request)
    return redirect('principal:inicio')


from django.http import JsonResponse

def user (request):
    if request.user.is_authenticated():
        data = {
            "log" : False,
        }
    
    try:
        user = Usuario.objects.get(username = request.user.username)
        data = {
            "log" : True,
            "img" : user.photo.url,
        }
    except Exception as e:
        data = {
            "log" : False,
        }

    return JsonResponse(data, safe = False)



def profile(request, username):
    try:
        user = Usuario.objects.get(username=username)

        if (user.groups.filter(name="artist").exists()):
            artist = True
            user = UserArtist.objects.get(username=username)
        else:
            artist = False

        follow = user.get_followers.filter(username = request.user.username).exists()

    except Exception as e:
        return HttpResponseBadRequest()



    if (request.is_ajax()):
        response = {
            "html" : render(request, "usuarios/profile.html", {'usuario':user, 'artist': artist, 'follow':follow}).getvalue().decode("utf-8") ,
            "success" : True,
            "get": True,
            "url":"/usuarios/profile/{0}".format(username)
        }

        return JsonResponse(response, safe=False) 
    else:       
        return render(request, "usuarios/profile.html", {'usuario':user, 'artist': artist, 'follow':follow})


def editProfile(request, username):
    
    try:
        user = Usuario.objects.get(username=username)
    except Exception as e:
        return HttpResponseBadRequest()

    if user.username != request.user.username:    
        return HttpResponseBadRequest()

    form = UsuarioCompleteForm(data=request.POST or None, files=request.FILES or None, instance = user)

    action = "/usuarios/profile/{0}/editar".format(username)

    if request.method == 'POST':
        
        if (form.is_valid()):

            user = form.save(commit = False)
            user.save()

            return redirect("/usuarios/profile/{0}".format(username))

        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, "usuarios/userEdit.html", {'form':form, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, "usuarios/userEdit.html", {'form':form, 'action':action})
    else:
        if (request.is_ajax()):
            response = {
                "html" : render(request, "usuarios/userEdit.html", {'form':form, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }

            return JsonResponse(response, safe=False) 
        else:       
            return render(request, "usuarios/userEdit.html", {'form':form, 'action':action})


def editArtistProfile(request, username):
    
    try:
        user = UserArtist.objects.get(username=username)
    except Exception as e:
        return HttpResponseBadRequest()

    if user.username != request.user.username:    
        return HttpResponseBadRequest()

    form = ArtistCompleteForm(data=request.POST or None, files=request.FILES or None, instance = user)

    action = "/usuarios/profile/{0}/aeditar".format(username)

    if request.method == 'POST':

        if (form.is_valid()):

            user = form.save(commit = False)
            user.save()

            return redirect("/usuarios/profile/{0}".format(username))

        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, "usuarios/userArtistEdit.html", {'form':form, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                return render(request, "usuarios/userArtistEdit.html", {'form':form, 'action':action})
    
    else:
        if (request.is_ajax()):
            response = {
                "html" : render(request, "usuarios/userArtistEdit.html", {'form':form, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }

            return JsonResponse(response, safe=False) 
        else:       
            return render(request, "usuarios/userArtistEdit.html", {'form':form, 'action':action})



from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def follow_user(request):
    
    id = request.POST.get('id')
    artist = request.POST.get('artist')

    try:
        userlog = Usuario.objects.get(username=request.user.username)

        if (artist):
            user = UserArtist.objects.get(pk=id)
        else:
            user = Usuario.objects.get(pk=id)

        created = userlog.add_relationship(user,1)

        if (not created):
            userlog.remove_relationship(user,1)
            if (artist):
                user.singer.followers.remove(userlog)
            response = {
                "r" : True,
                "add" : False
            }
        else:
            if (artist):
                user.singer.followers.add(userlog)
            response = {
                "r" : True,
                "add" : True
            }

    except:
        response = {
            "r" : False,
        }    
    
    return JsonResponse(response, safe = False)







# ------ ALBUMS desde perfil------

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count, Sum
def albums(request, username):
    try:
        user = UserArtist.objects.get(username=username)

        query = user.singer.album_set.all().annotate(num_canciones=Count('song')).annotate(num_views=Count('song__view'))
        

    except Exception as e:
        return HttpResponseBadRequest()

    if ( query is not None):
        page = request.GET.get('page', 1)

        paginator =  Paginator(query, 10)

        try:
            albums = paginator.page(page)
        except PageNotAnInteger:
            albums = paginator.page(1)
        except EmptyPage:
            albums = paginator.page(paginator.num_pages)
    else:
        albums = query

    return render(request, "song/albumList.html", {'albums':albums, 'username':username})

def albumList(request, username, id):

    try:
        album = Album.objects.get(id = id)

        relacionados = Album.objects.all().order_by('?')[:3]
    except Exception as e:
        return HttpResponseBadRequest()

    return render(request, "song/listaAlbum.html", {'album':album, 'username':username, 'relacionados':relacionados})


def playlists(request, username):
    try:
        user = Usuario.objects.get(username=username)

        query = user.playlist_set.all().annotate(num_canciones=Count('songs')).annotate(num_views=Count('view'))

    except Exception as e:
        return HttpResponseBadRequest()

    if ( query is not None):
        page = request.GET.get('page', 1)

        paginator =  Paginator(query, 10)

        try:
            playlists = paginator.page(page)
        except PageNotAnInteger:
            playlists = paginator.page(1)
        except EmptyPage:
            playlists = paginator.page(paginator.num_pages)
    else:
        playlists = query

    return render(request, "song/playList.html", {'playLists':playlists, 'username':username})

def playList(request, username, id):

    try:
        playlist = PlayList.objects.get(id = id)
        relacionados = PlayList.objects.all().order_by('?')[:3]
    except Exception as e:
        return HttpResponseBadRequest()

    return render(request, "song/listaPlay.html", {'playlist':playlist, 'username':username, 'relacionados':relacionados})


def favorites(request, username):
    
    try:
        user = Usuario.objects.get(username=username)

        query = user.song_set.all().annotate(num_views=Count('view')).all().order_by('-release_date')

    except Exception as e:
        return HttpResponseBadRequest()

    if ( query is not None):
        page = request.GET.get('page', 1)

        paginator =  Paginator(query, 10)

        try:
            canciones = paginator.page(page)
        except PageNotAnInteger:
            canciones = paginator.page(1)
        except EmptyPage:
            canciones = paginator.page(paginator.num_pages)
    else:
        canciones = query

    return render(request, "song/favorites.html", {'canciones':canciones})

def songs(request, username):
    
    try:
        user = UserArtist.objects.get(username=username)

        query = user.singer.song_set.all().annotate(num_views=Count('view')).order_by('-release_date')

    except Exception as e:
        return HttpResponseBadRequest()

    if ( query is not None):
        page = request.GET.get('page', 1)

        paginator =  Paginator(query, 10)

        try:
            canciones = paginator.page(page)
        except PageNotAnInteger:
            canciones = paginator.page(1)
        except EmptyPage:
            canciones = paginator.page(paginator.num_pages)
    else:
        canciones = query

    return render(request, "song/mySongs.html", {'canciones':canciones, 'username':username})



def dowmloads(request, username):
    try:
        user = Usuario.objects.get(username=request.user.username)

        query = user.downloads.all().annotate(num_views=Count('view'))
    except Exception as e:
        return HttpResponseBadRequest()

    if ( query is not None):
        page = request.GET.get('page', 1)

        paginator =  Paginator(query, 10)

        try:
            canciones = paginator.page(page)
        except PageNotAnInteger:
            canciones = paginator.page(1)
        except EmptyPage:
            canciones = paginator.page(paginator.num_pages)
    else:
        canciones = query

    return render(request, "song/myDownloads.html", {'canciones':canciones, 'username':username})


def following(request, username):
    
    try:
        user = Usuario.objects.get(username=username)
        query = user.get_following

    except Exception as e:
        return HttpResponseBadRequest()

    if ( query is not None):
        page = request.GET.get('page', 1)

        paginator =  Paginator(query, 10)

        try:
            seguidos = paginator.page(page)
        except PageNotAnInteger:
            seguidos = paginator.page(1)
        except EmptyPage:
            seguidos = paginator.page(paginator.num_pages)
    else:
        seguidos = query

    return render(request, "song/following.html", {'seguidos':seguidos, 'username':username, 'title':'Seguidos'})



def followers(request, username):
    try:
        user = Usuario.objects.get(username=username)
        query = user.get_followers

    except Exception as e:
        return HttpResponseBadRequest()

    if ( query is not None):
        page = request.GET.get('page', 1)

        paginator =  Paginator(query, 10)

        try:
            seguidos = paginator.page(page)
        except PageNotAnInteger:
            seguidos = paginator.page(1)
        except EmptyPage:
            seguidos = paginator.page(paginator.num_pages)
    else:
        seguidos = query

    return render(request, "song/following.html", {'seguidos':seguidos, 'username':username, 'title':'Seguidores'})























