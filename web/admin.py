from django.contrib import admin

# Register your models here.
from .models import *

class AdminUsuario(admin.ModelAdmin):
    list_filter = ["date_joined"]
    search_fields = ["email", "username"]
    class Meta:
        model = Usuario
    
admin.site.register(Usuario, AdminUsuario)


class AdminArtist(admin.ModelAdmin):
    list_filter = ["date_joined"]
    search_fields = ["email", "username"]
    class Meta:
        model = UserArtist
    
admin.site.register(UserArtist, AdminArtist)



