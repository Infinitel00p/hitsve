from django.core.exceptions import ValidationError
 
def valid_extension(value):
    if (not value.name.endswith('.png') and
        not value.name.endswith('.jpeg') and 
        not value.name.endswith('.gif') and
        not value.name.endswith('.bmp') and 
        not value.name.endswith('.jpg')):
 
        raise ValidationError("Archivos permitidos: .jpg, .jpeg, .png, .gif, .bmp")
    
def valid_extension_mp3(value):
    if (not value.name.endswith('.mp3')):
        raise ValidationError("Archivos permitidos: .mp3")
    
    
from django.contrib.auth.models import User

def validate_email_unique(value):
    exists = User.objects.filter(email=value)
    if exists:
        raise ValidationError("Email address %s already exits, must be unique" % value)