"""hits URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth.views import login 
from web import views

from django.contrib.auth.decorators import login_required
#from .views import Sign_in

urlpatterns = [
    #url(r'^login$', login,{'template_name':'usuarios/login.html'}, name = 'login'),
    url(r'^login$', views.log_in, name = 'login'),
    url(r'^register$', views.register, name = 'register_select'),
    url(r'^register/usuario$', views.sign_up, name = 'register'),
    url(r'^register/musico$', views.sign_up_artist, name = 'register_musician'),
    url(r'^logout', login_required(views.log_out), name='logout'),

    url(r'^ajax/user$', views.user, name='img_user'),

    url(r'^profile/(?P<username>\w+)$', views.profile, name='profile'),
    url(r'^profile/(?P<username>\w+)/editar$', login_required(views.editProfile), name='edit_profile'),
    url(r'^profile/(?P<username>\w+)/aeditar$', login_required(views.editArtistProfile), name='edit_artist_profile'),


    url(r'^ajax/followuser$', login_required(views.follow_user), name='follow_user'),

    url(r'^profile/(?P<username>\w+)/albums$', views.albums, name='albums_user'),
    url(r'^profile/(?P<username>\w+)/playlists$', views.playlists, name='playlists_user'),
    url(r'^profile/(?P<username>\w+)/favoritas$', views.favorites, name='favorites_user'),
    url(r'^profile/(?P<username>\w+)/albums/(?P<id>\d+)$', views.albumList, name='albumList_user'),
    url(r'^profile/(?P<username>\w+)/playlists/(?P<id>\d+)$', views.playList, name='playList_user'),
    url(r'^profile/(?P<username>\w+)/canciones$', views.songs, name='songs_user'),
    url(r'^profile/(?P<username>\w+)/descargas$', login_required(views.dowmloads), name='downloads_user'),
    url(r'^profile/(?P<username>\w+)/seguidos$', login_required(views.following), name='following_user'),
    url(r'^profile/(?P<username>\w+)/seguidores$', login_required(views.followers), name='followers_user'),
]
