from django.db import models
from django.contrib.auth.models import User
from django_countries.fields import CountryField
import os
from .validators import valid_extension

from django.conf import settings
from song.models import Artist
# Create your models here.


def generate_path(instance, filename):
    folder = "modelo_" + str(instance.user) 
    return os.path.join("adjuntos", folder, filename)   
    
class Usuario(User):
    country = CountryField()
    birthdate = models.DateField(null = False, blank = False)
    photo = models.ImageField(blank=True, null=True, upload_to=settings.URL_MYMEDIA+'users/photo', validators=[valid_extension], default = 'static/images/default-profile.png')
    front = models.ImageField(blank=True, null=True, upload_to=settings.URL_MYMEDIA+'users/front', validators=[valid_extension],default = 'static/images/default-post.jpg')
    description = models.TextField(blank = True, null = True)
    name = models.CharField(max_length=150, blank = True, null = True)
    website = models.URLField(blank = True, null = True)
    instagram = models.CharField(max_length=150, blank = True, null = True)

    relationships = models.ManyToManyField("self", symmetrical = False, blank = True, through='Relationship', related_name='related_to')

    def add_relationship(self, person, status):
        relationship, created = Relationship.objects.get_or_create(
            from_person=self,
            to_person=person,
            status=status)
        return created

    def remove_relationship(self, person, status):
        Relationship.objects.filter(
            from_person=self,
            to_person=person,
            status=status).delete()
        return

    def get_relationships(self, status):
        return self.relationships.filter(
            to_people__status=status,
            to_people__from_person=self)

    def get_related_to(self, status):
        return self.related_to.filter(
            from_people__status=status,
            from_people__to_person=self)

    @property
    def get_following(self):
        return self.get_relationships(RELATIONSHIP_FOLLOWING)

    @property
    def get_followers(self):
        return self.get_related_to(RELATIONSHIP_FOLLOWING)

    def get_friends(self):
        return self.relationships.filter(
            to_people__status=RELATIONSHIP_FOLLOWING,
            to_people__from_person=self,
            from_people__status=RELATIONSHIP_FOLLOWING,
            from_people__to_person=self)


RELATIONSHIP_FOLLOWING = 1
RELATIONSHIP_BLOCKED = 2
RELATIONSHIP_STATUSES = (
    (RELATIONSHIP_FOLLOWING, 'Following'),
    (RELATIONSHIP_BLOCKED, 'Blocked'),
)

class Relationship(models.Model):
    from_person = models.ForeignKey(Usuario, related_name='from_people')
    to_person = models.ForeignKey(Usuario, related_name='to_people')
    status = models.IntegerField(choices=RELATIONSHIP_STATUSES)
    
class UserArtist(Usuario): 
    
    stage_name = models.CharField(max_length=30)
    singer = models.OneToOneField(Artist, on_delete=models.DO_NOTHING, blank = True, null = True, related_name='artist')
    
    def save(self, *args, **kwargs):
        if(self.singer is None):
            artist = Artist()
            artist.name = self.stage_name
            artist.save()
            self.singer = artist

        super(UserArtist, self).save(*args, **kwargs)
        self.singer.save()

User._meta.get_field('email').blank = False
    

        