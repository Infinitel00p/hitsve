# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-16 22:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0016_auto_20170614_1248'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
    ]
