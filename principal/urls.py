"""hits URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth.views import login 
from principal import views
#from .views import Sign_in

from django.contrib.auth.decorators import permission_required, login_required

urlpatterns = [
    #url(r'^login$', login,{'template_name':'usuarios/login.html'}, name = 'login'),
    url(r'^$', views.start, name = 'inicio'),
    url(r'^menu$', views.menu, name = 'menu'),
    url(r'^buscar$', views.search, name = 'search'),
    url(r'^canciones$', views.songs, name = 'song'),
    url(r'^artistas$', views.artists, name = 'artist'),    
    url(r'^noticias$', views.notices, name = 'notice'),    
    url(r'^albums$', views.albums, name = 'album'),  
    url(r'^playlists$', views.playlists, name = 'playlists'),

    url(r'^terminos$',(views.terminos), name='terminos'),
    

    #-- banner princial
    url(r'^bann/lista$', permission_required('principal.add_advertising')(views.listBanner), name='listar_banner'),
    url(r'^bann/eliminar/(?P<id>\d+)$', permission_required('principal.add_advertising')(views.deleteBanner), name='eliminar_banner'),
    url(r'^bann/editar/(?P<id>\d+)$', permission_required('principal.add_advertising')(views.editBanner), name='editar_banner'),
    url(r'^bann/crear$', permission_required('principal.add_advertising')(views.createBanner), name='crear_banner'),
    
]
