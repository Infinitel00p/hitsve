from django.contrib import admin

# Register your models here.
from .models import *

class AdminAdvertising(admin.ModelAdmin):
    list_display = ['id', 'link', 'category']
    search_fields = ["id",'category']
    class Meta:
        model = Advertising
    
admin.site.register(Advertising, AdminAdvertising)

class AdminCategory(admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ["name"]
    class Meta:
        model = Category
    
admin.site.register(Category, AdminCategory)
