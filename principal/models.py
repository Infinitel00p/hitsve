from django.db import models
from django.conf import settings

from web.validators import valid_extension
# Create your models here.

class Advertising(models.Model):
    id = models.AutoField(primary_key=True)
    photo = models.ImageField(blank=False, null=False, upload_to=settings.URL_MYMEDIA+'advertising', validators=[valid_extension])
    link = models.CharField(max_length=150)
    title = models.CharField(max_length=100,blank=False, null=False, default="")
    category = models.ForeignKey('principal.Category', on_delete=models.CASCADE )
    
    
        
class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, unique = True)
    
    def __unicode__(self):
        self.name
        
    def __str__(self):
        return '{0}'.format(self.name)