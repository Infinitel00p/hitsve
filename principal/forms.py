
from django import forms
from .models import *
from django.forms.widgets import Widget

class AdvertisingForm (forms.ModelForm):


    class Meta:
        model = Advertising
        
        field = (
                'photo',
                'link',
                'category',
                'title',
            )
        
        labels = {
                'photo' : 'Banner',
                'link' : 'link',
                'category' : 'Categoría',

                'title' : 'Title'
            }

        widgets = {
            'photo' : forms.FileInput(attrs = {'class':'mi clases'}),
            'link' : forms.TextInput(),
            'title' : forms.TextInput(),
            'category' : forms.Select(attrs = {'class':'select-form'}),            
            }
        
        exclude = ['id']


class CategoryForm (forms.ModelForm):


    class Meta:
        model = Category
        
        field = (
                'name',
            )
        
        labels = {
                'name':'Nombre',

            }

        widgets = {
            'name' : forms.TextInput(attrs = {'class':'mi clases'}),
            }
        
        exclude = ['id']

    def clean_name(self):
        name = self.cleaned_data.get('name')

        if Category.objects.filter(name=name).exists():
            raise forms.ValidationError("Este nombre de categoría ya esta en uso.")

        return name