#!/usr/bin/python
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, Http404,JsonResponse 
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
# Create your views here.
from .models import *
from .forms import *
from django.contrib.messages.api import success
from django.urls.base import reverse_lazy
# Create your views here.
from web.models import UserArtist, Usuario

from post.models import Post

from django.contrib.auth.decorators import permission_required, login_required

from song.models import *

from django.db.models import Q
from django.db.models import Count
# -------- Administrador --------------    
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
import json

DEVELOPER_KEY = "AIzaSyCIM4EzNqi1in22f4Z3Ru3iYvLaY8tc3bo"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

def start(request):
	today = datetime.now()
	backward = today - timedelta(days=7)

	print(" ---------- ")
	print()
	print("  -------- ")

	if (request.META["HTTP_HOST"].split(".")[0] == "player"):
		return HttpResponseBadRequest() 

	youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,developerKey=DEVELOPER_KEY)
	search_response = youtube.search().list(
	    type="video",
	    part="snippet",
	    fields="items(id/videoId,snippet/title,snippet/thumbnails/high/url)",
	    maxResults=2,
		order="viewCount",
	    publishedBefore=today.isoformat('T')+"Z",
	    publishedAfter=backward.isoformat('T')+"Z",
	    videoCategoryId=10,

	  ).execute()

	top_hits = []
	for result in search_response.get("items", []):
		name = result["snippet"]["title"].replace("Official","").replace("Music","").replace("Video","").replace("(","").replace(")","")
		n = name.split('-')
		try:
			title = n[1]
		except Exception as e:
			title = ""

		vr = {
			'id':result["id"]["videoId"],
			'title':title,
			'artist':n[0],
			'name':name,
			'thumb':result["snippet"]["thumbnails"]["high"]["url"]
		}
		top_hits.append(vr)


	search_response = youtube.search().list(
	    type="video",
	    part="snippet",
	    fields="items(id/videoId,snippet/title,snippet/thumbnails/high/url)",
	    maxResults=8,
	    publishedBefore=today.isoformat('T')+"Z",
	    publishedAfter=backward.isoformat('T')+"Z",
	    videoCategoryId=10,

	  ).execute()

	mas_nuevo = []
	for result in search_response.get("items", []):
		name = result["snippet"]["title"].replace("Official","").replace("Music","").replace("Video","").replace("(","").replace(")","")
		n = name.split('-')
		try:
			title = n[1]
		except Exception as e:
			title = ""

		vr = {
			'id':result["id"]["videoId"],
			'title':title,
			'artist':n[0],
			'name':name,
			'thumb':result["snippet"]["thumbnails"]["high"]["url"]
		}
		mas_nuevo.append(vr)

	ultimas_noticias = Post.objects.all().order_by('-time_stamp')[:10]
	noticias_populares = Post.objects.all().order_by('views')[:3]
	tops = Post.objects.all().filter(category__id=6).order_by('views')[:4]
	noticias = Post.objects.all().filter(category__id=5).order_by('views')[:4]
	curiosidades = Post.objects.all().filter(category__id=7).order_by('views')[:4]
	estrenos = Post.objects.all().filter(category__id=7).order_by('views')[:4]
	banners = Advertising.objects.all()

	context = {'mas_nuevo': mas_nuevo,
				'top_hits':top_hits,
			   'ultimas_noticias': ultimas_noticias,
			   'noticias_populares': noticias_populares,
			   'tops':tops,
			   'estrenos':estrenos,
			   'noticias':noticias,
			   'curiosidades':curiosidades,
			   'banners':banners
	}

	if (request.is_ajax()):
		response = {
		    "html" : render(request, 'principal/inicio.html', context ).getvalue().decode("utf-8") ,
		    "success" : True,
		    "get": True,
		    "url":"/"
		}

		return JsonResponse(response, safe=False) 
	else:       
		return render(request, 'principal/inicio.html', context )


def search(request):
	
	search = request.GET.get("search")

	if (search != "" and search is not None):

		noticias = Post.objects.filter(Q(title__icontains=search) | Q(category__name__icontains=search)).order_by('-time_stamp')[:6]

		canciones = Song.objects.filter(Q(artists__name__icontains=search) | Q(name__icontains=search) | Q(album__name__icontains=search)).order_by('-release_date')[:6]

		artists = Artist.objects.filter(name__icontains=search).order_by('name')[:6]

		albums = Album.objects.filter(Q(name__icontains=search) | Q(artist__name__icontains=search)).order_by('name')[:6]

		playlists =  PlayList.objects.filter(Q(name__icontains=search) | Q(user__username__icontains=search) | Q(user__name__icontains=search)).order_by('name')
	
		
	else:
		noticias = None
		canciones = None
		artists = None
		albums = None
		playlists = None

	context = {
		'noticias':noticias,
		'canciones':canciones,
		'artistas':artists,
		'albums':albums,
		'playlists':playlists
	}

	if (request.is_ajax()):
		response = {
		    "html" : render(request, 'principal/buscar.html',context) .getvalue().decode("utf-8") ,
		    "success" : True,
		    "get": True,
		    "url":"/buscar?search={0}".format(search)
		}

		return JsonResponse(response, safe=False) 
	else:       
		return render(request, 'principal/buscar.html',context) 

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime, date, time, timedelta

def songs(request):
	
	search = request.GET.get("search")
	top = request.GET.get("top")
	
	if (search != "" and search is not None):
		query = Song.objects.annotate(num_views=Count('view')).filter(Q(artists__name__icontains=search) | Q(name__icontains=search) | Q(album__name__icontains=search)).order_by('-release_date')
	else:
		if (request.GET.get("top") is not None and str(top) == "true"):
			backward = date.today() - timedelta(days=30)
			query = Song.objects.annotate(num_views=Count('view')).filter(release_date__range=[backward,date.today()]).order_by('-num_views')
		else:
			if ((search == "" or search is None) and request.GET.get("top") == "" or top is None):
				query = Song.objects.annotate(num_views=Count('view')).all().order_by('-release_date')
			else:
				query = None

	if ( query is not None):
		page = request.GET.get('page', 1)

		paginator =  Paginator(query, 10)

		try:
			canciones = paginator.page(page)
		except PageNotAnInteger:
			canciones = paginator.page(1)
		except EmptyPage:
			canciones = paginator.page(paginator.num_pages)
	else:
		canciones = query

	return  render(request, 'principal/buscarCancion.html',{'canciones':canciones})


def artists(request):
	search = request.GET.get("search")
	
	populares = request.GET.get("populares")

	if (search != "" and search is not None and (populares == "" or populares is None)):
		query =  Artist.objects.filter(name__icontains=search).order_by('name')
	else:
		if ((search == "" or search is None) and (populares == "" or populares is None)):
			query =  Artist.objects.all().order_by('name')
		else:
			if ((search == "" or search is None) and populares == "true"):
				query = Artist.objects.all().order_by('-followers')
			else:
				query = None

	if ( query is not None):
		page = request.GET.get('page', 1)

		paginator =  Paginator(query, 10)

		try:
			artistas = paginator.page(page)
		except PageNotAnInteger:
			artistas = paginator.page(1)
		except EmptyPage:
			artistas = paginator.page(paginator.num_pages)
	else:
		artistas = query

	return  render(request, 'principal/buscarArtista.html',{'artistas':artistas})


def albums(request):
	search = request.GET.get("search")
	
	populares = request.GET.get("populares")

	if (search != "" and search is not None and (populares == "" or populares is None)):
		query =  Album.objects.filter(Q(name__icontains=search) | Q(artist__name__icontains=search)).annotate(num_canciones=Count('song')).annotate(num_views=Count('song__view')).order_by('name')
	else:
		if ((search == "" or search is None)):
			query =  Album.objects.all().order_by('name')
		else:
			query = None

	if ( query is not None):
		page = request.GET.get('page', 1)

		paginator =  Paginator(query, 10)

		try:
			albums = paginator.page(page)
		except PageNotAnInteger:
			albums = paginator.page(1)
		except EmptyPage:
			albums = paginator.page(paginator.num_pages)
	else:
		albums = query

	return  render(request, 'principal/buscarAlbum.html',{'albums':albums})



def playlists(request):
	search = request.GET.get("search")
	
	populares = request.GET.get("populares")

	if (search != "" and search is not None):
		query =  PlayList.objects.filter(Q(name__icontains=search) | Q(user__username__icontains=search) | Q(user__name__icontains=search)).annotate(num_canciones=Count('songs')).annotate(num_views=Count('songs__view')).order_by('name')
	else:
		if ((search == "" or search is None)):
			query =  PlayList.objects.all().order_by('name')
		else:
			query = None

	if ( query is not None):
		page = request.GET.get('page', 1)

		paginator =  Paginator(query, 10)

		try:
			playlists = paginator.page(page)
		except PageNotAnInteger:
			playlists = paginator.page(1)
		except EmptyPage:
			playlists = paginator.page(paginator.num_pages)
	else:
		playlists = query

	return  render(request, 'principal/buscarPlaylist.html',{'playlists':playlists})


def notices(request):
	search = request.GET.get("search")
	if (search != "" and search is not None):
		query =  Post.objects.filter(Q(title__icontains=search) | Q(category__name__icontains=search)).order_by('-time_stamp')
	else:
		if (search == "" or search is None):
			query =  Post.objects.all().order_by('-time_stamp')
		else:
			query = None

	if ( query is not None):
		page = request.GET.get('page', 1)

		paginator =  Paginator(query, 10)

		try:
			noticias = paginator.page(page)
		except PageNotAnInteger:
			noticias = paginator.page(1)
		except EmptyPage:
			noticias = paginator.page(paginator.num_pages)
	else:
		noticias = query

	return  render(request, 'principal/buscarNoticia.html',{'noticias':noticias,'search':search})

def menu(request):
	
	print(request.user.username)
	
	return render(request, 'menu.html',{}) 



def listBanner(request):
    
	if (request.method == "GET"):
		objs = Advertising.objects.all()
		if (request.is_ajax()):
			response = {
				"html" : render(request, "principal/admin/listBanner.html", {'object_list':objs}).getvalue().decode("utf-8") ,
				"success" : True,
				"get": True,
				"url":"/bann/lista"
			}
			return JsonResponse(response, safe=False)
		else:
			return render(request, "principal/admin/listBanner.html", {'object_list':objs})

	else:
		HttpResponseBadRequest()

def editBanner(request, id):
	try:
		b = Advertising.objects.get(id=id)
	except Exception as e:
		return HttpResponseBadRequest()

	title = "Editar Banner"
	action = "/bann/editar/{0}".format(b.id)

	if request.method == 'GET':
		form = AdvertisingForm(instance=b)
		form.fields['photo'].required = False
    
		if (request.is_ajax()):
			response = {
				"html" : render(request, 'principal/admin/create.html', {'form':form,'photo':b.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
				"success" : True,
				"get": True,
				"url":action
			}
			return JsonResponse(response, safe=False) 
		else:
			return render(request, 'principal/admin/create.html', {'form':form, 'photo':b.photo, 'title':title, 'action':action })
	else:
		form = AdvertisingForm(request.POST or None, request.FILES or None, instance = b)
		form.fields['photo'].required = False
        
		if form.is_valid():
			banner = form.save(commit=False)

			banner.save()

			return redirect('principal:listar_banner')
		else:
			if (request.is_ajax()):
				response = {
					"html" : render(request, 'principal/admin/create.html', {'form':form, 'photo':b.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
					"success" : True
				}
				return JsonResponse(response, safe=False) 
			else:
				return render(request, 'principal/admin/create.html', {'form':form, 'photo':b.photo, 'title':title, 'action':action})
    
def createBanner(request):

	title = "Crear Banner"
	action = "/bann/crear"

	if request.method == 'GET':
		form = AdvertisingForm()
    
		if (request.is_ajax()):
			response = {
				"html" : render(request, 'principal/admin/create.html', {'form':form,'photo':'', 'title':title, 'action':action}).getvalue().decode("utf-8") ,
				"success" : True,
				"get": True,
				"url":action
			}
			return JsonResponse(response, safe=False) 
		else:
			return render(request, 'principal/admin/create.html', {'form':form, 'photo':'', 'title':title, 'action':action })
	else:
		form = AdvertisingForm(request.POST or None, request.FILES or None)

		if form.is_valid():
			banner = form.save(commit=False)

			banner.save()

			return redirect('principal:listar_banner')
		else:
			if (request.is_ajax()):
				response = {
					"html" : render(request, 'principal/admin/create.html', {'form':form, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
					"success" : True
				}
				return JsonResponse(response, safe=False) 
			else:
				return render(request, 'principal/admin/create.html', {'form':form, 'title':title, 'action':action})

def deleteBanner(request, id):
	try:
		b = Advertising.objects.get(id=id)

		b.delete()

		return redirect('principal:listar_banner')
	except Exception as e:
		HttpResponseBadRequest()


def terminos(request):
	return render(request, 'terminos.html', {})