from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, JsonResponse 
from django.views.generic import ListView
from django.urls import reverse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
# Create your views here.
from .forms import *
from .models import *
from song.models import Artist
from django.contrib.messages.api import success
from audioop import reverse
from django.urls.base import reverse_lazy
# Create your views here.
from web.views import Usuario

from django.contrib.auth.decorators import permission_required, login_required

class ListCategory(ListView):
    model = Category
    template_name = 'post/categories/list.html'

from django.utils.decorators import method_decorator

class CreateCategory(CreateView):
    model = Category
    form_class = CategoryForm
    template_name = 'post/categories/create.html'
    success_url = reverse_lazy('noticias:listar_categorias')
    
    #@permission_required('post.add_category', raise_exception=True)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateCategory, self).dispatch(request, *args, **kwargs)
    
    """
    def form_valid(self, form_class):
        form_class.instance.artist = self.request.user.id
        return super(CreateCategory, self).form_valid(form_class)
"""

class ListPost(ListView):
    model = Post
    template_name = 'post/posts/list.html'
    
    def get_queryset(self):
        queryset = super(ListPost, self).get_queryset()
        autor = Usuario.objects.get(username=self.request.user.username)
        return queryset.filter(author=autor)
    
from django.forms.utils import ErrorList
    
class CreatePost(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'post/posts/create.html'
    success_url = reverse_lazy('noticias:listar_noticias')
    photo = ""
 
    def form_valid(self, form_class):
        usuario = Usuario.objects.get(username=self.request.user.username)
        form_class.instance.author = usuario #self.request.user.id

        if form_class.instance.photo.__str__() == '':
            form_class._errors['photo'] = ErrorList([
                    u'Seleciona una imagen de portada'
                ])
            return self.form_invalid(form_class)
        
        published = self.request.POST.get('value')
        if published == "p" :
            form_class.instance.published = True
        else :
            form_class.instance.published = False
           
        return super(CreatePost, self).form_valid(form_class)

    def get_context_data(self,*args, **kwargs):
        ctx = super(CreatePost, self).get_context_data(*args,**kwargs)
        ctx['photo'] = self.photo
        ctx['title'] = "Nueva noticia"
        ctx["action"] = "/noticias/nueva/"
        return ctx

    def get_form(self, form_class=None):
        form = super(CreatePost, self).get_form(form_class)
        # the actual modification of the form
        form.fields['category'].required = False

        return form
from django.core import serializers


def updatePost(request, id):

    try:
        user = Usuario.objects.get(username=request.user.username) 
        post = Post.objects.get(id = id)
    except:
        return redirect('noticias:listar_noticias')
    
    if (not (user == post.author)):
        return redirect('noticias:listar_noticias')
    
    title = "Editar {0}".format(post.title)
    action = "/noticias/editar/{0}/".format(post.id)

    if request.method == 'GET':
        form = PostForm(request.FILES or None,instance=post)
        form.fields['category'].required = False
        if (request.is_ajax()):
            response = {
                "html" : render(request, 'post/posts/create.html', {'form':form, 'photo':post.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                "success" : True,
                "get": True,
                "url":action
            }
            return JsonResponse(response, safe=False) 
        else:
            return render(request, 'post/posts/create.html', {'form':form, 'photo':post.photo, 'title':title, 'action':action})
    else:
        form = PostForm(request.POST or None,request.FILES or None, instance = post)
        form.fields['category'].required = False

        if form.is_valid():
            post = form.save(commit = False)
            
            if (not post.published):
                published = request.POST.get('value')
                if published == "p" :
                    post.published = True
                else :
                    post.published = False
            
            post.save()

            return redirect('noticias:listar_noticias')
        else:
            if (request.is_ajax()):
                response = {
                    "html" : render(request, 'post/posts/create.html', {'form':form, 'photo':post.photo, 'title':title, 'action':action}).getvalue().decode("utf-8") ,
                    "success" : True
                }
                return JsonResponse(response, safe=False) 
            else:
                render(request, 'post/posts/create.html', {'form':form, 'photo':post.photo, 'title':title, 'action':action})

class DeletePost(DeleteView):
    model = Post
    form_class = PostForm
    template_name = 'post/posts/delete.html'
    success_url = reverse_lazy('noticias:listar_noticias')
    
    def get_object(self, queryset=None):
        user = Usuario.objects.get(username=self.request.user.username) 
        post = Post.objects.get(id = self.kwargs['pk'])
        
        if user != post.author :
            return None
        else :
            return post
    
    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.kwargs = kwargs
        self.object = self.get_object()
        
        if (self.object is None):
            return redirect('noticias:listar_noticias')
        # this is going to call self.get_object again isn't it?
        return UpdateView.dispatch(self, request, *args, **kwargs)

    def get_context_data(self,*args, **kwargs):
        ctx = super(DeletePost, self).get_context_data(*args,**kwargs)
        ctx["action"] = "/noticias/eliminar/{0}/".format(self.get_object().id)
        return ctx
    
from next_prev import next_in_order, prev_in_order
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
def detail(request, slug):
    try:
        post = Post.objects.get(slug=slug)
    except :
        return HttpResponseBadRequest()

    nextP = next_in_order(post)
    prev = prev_in_order(post, loop=True)

    print(nextP.title)
    print(prev.title)

    relacionados = Post.objects.order_by('-time_stamp')[:6]

    similar = Post.objects.filter(category = post.category).order_by('-time_stamp')[:3]

    page = request.GET.get('page', 1)

    paginator =  Paginator(post.comment_set.all().order_by("-time_stamp"), 10)


    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        comments = paginator.page(1)
    except EmptyPage:
        comments = paginator.page(paginator.num_pages)
    
    response = render(request, 'post/posts/postnoticia.html', {'post':post, 'comments':comments, 'relacionados':relacionados,'similar':similar})

    return response

#muerta vista solo con comentarios
def comment_pag(request):

    id = request.GET.get('id')

    try:
        post = Post.objects.get(id=id)
    except :
        return HttpResponseBadRequest()

    page = request.GET.get('page', 1)

    paginator =  Paginator(post.comment_set.all().order_by("-time_stamp"), 10)

    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        comments = paginator.page(1)
    except EmptyPage:
        comments = paginator.page(paginator.num_pages)

    return render(request, 'post/posts/comments_ajax.html', { 'comments':comments, 'post_id':id})



from django.views.decorators.csrf import csrf_protect

from django.utils.timesince import timesince
#from django.core import serializers
import json
#Vista agrega un nuevo comentario ajax
def comment(request):

    try:
        user = Usuario.objects.get(username=request.user.username) 
        post_r = Post.objects.get(id = request.POST.get('id'))
    except:
        return redirect('noticias:listar_noticias')
    
    if request.is_ajax():    
        content = request.POST.get("content")

        if content:
            comment = Comment();
            
            comment.author = user
            comment.post = post_r
            comment.content = content

            comment.save()
            comentario = {
                'error':True,
                'content':content,
                'time':str(timesince(comment.time_stamp)),
                'photo':str(comment.author.photo),
                'name':str(comment.author.username),
                'id':str(comment.author.pk),
            }
            #comentario_serialized = serializers.serialize('json', comentario)
            return JsonResponse(comentario, safe=False)
        else:
            error = {
                'error':False,
                'errorlist':'Escribe algo!!'
            }
            return JsonResponse(error, safe=False)


from django.views.decorators.csrf import csrf_exempt
@csrf_exempt
def like_post(request):
    
    id_post = request.POST.get('id_post')
    try:
        user = Usuario.objects.get(username=request.user.username)
        post = Post.objects.get(id = id_post)

        if (post.likes.all().filter(id = user.id).exists()):
            post.likes.remove(user)
            data = {
                'response': True,
                'like' : False
            }
        else:
            post.likes.add(user)
            data = {
                'response': True,
                'like' : True
            }
    except :
        data = { "response":False }
    
    return JsonResponse(data, safe = False)


def view_post(request):
    id_post = request.POST.get('id')

    try:
        post = Post.objects.get(id=id_post)
        post.views += 1
        post.save()
        data = True
    except Exception as e:
        data = False

    return JsonResponse(data, safe = False)



#views api rest 
from .serializers import PostSerializer
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

class PostsList(generics.ListAPIView):
    serializer_class = PostSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        param = self.request.query_params.get('dest', None)

        if param is not None:
            if param == 'true':
                queryset = Post.objects.all().order_by('views')
            else:
                queryset = Post.objects.all().order_by('-time_stamp')
        else:
            queryset = Post.objects.all().order_by('-time_stamp')
            
        return queryset


from django.db.models import Q

class PostsSearch(generics.ListAPIView):
    serializer_class = PostSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        param = self.request.query_params.get('q', None)
        
        if param is not None:
            queryset = Post.objects.all().filter(Q(title__icontains=param) | Q(category__name__icontains=param)).order_by('-time_stamp')
        else:
            queryset = Post.objects.all().order_by('-time_stamp')
            
        return queryset