from django.db import models
from web.validators import valid_extension
from django.template import defaultfilters
from web.models import Usuario
import os.path
from ckeditor_uploader.fields import RichTextUploadingField

from django.conf import settings
# Create your models here.

def generate_path(instance, filename):
    folder = "song_" + str(instance.user) 
    return os.path.join("posts", folder, filename)   


class Category (models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, unique = True)
    
    def __unicode__(self):
        self.name
        
    def __str__(self):
        return '{0}'.format(self.name)
    
class Post(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=150, unique=True)
    content = RichTextUploadingField()
    time_stamp = models.DateTimeField(auto_now_add=True)
    Update= models.DateField(auto_now_add=True)
    photo = models.ImageField(blank=True, null=False, upload_to=settings.URL_MYMEDIA+'posts', validators=[valid_extension])
    published = models.BooleanField(default = False, blank= True)
    views = models.IntegerField(default=0, blank=True)
    tags = models.CharField(max_length=500, null=True)
    
    author = models.ForeignKey(Usuario, null = False , blank = True, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, null = False, blank = False, on_delete=models.PROTECT)
   
    likes = models.ManyToManyField('web.Usuario',related_name='user_post_likes', symmetrical = True, blank=True)
    
    slug = models.SlugField(max_length=100, blank = True)
    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.title)
        super(Post, self).save(*args, **kwargs)

class Comment (models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField()
    time_stamp = models.DateTimeField(auto_now_add=True)
    
    author = models.ForeignKey(Usuario, null = False , blank = True, on_delete=models.DO_NOTHING)
    post = models.ForeignKey(Post, null = False , blank = True, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-time_stamp']
    