from .models import Post
from rest_framework.serializers import ModelSerializer,ReadOnlyField, Field
from django.utils.timesince import timesince

class TimestampField(Field):
    def to_representation(self, value):
        return str(timesince(value))

class PostSerializer(ModelSerializer):
	category_name = ReadOnlyField(source='category.name', read_only=True)
	author_name = ReadOnlyField(source='author.username', read_only=True)
	time_stamp = TimestampField( read_only=True)
	"""docstring for PostSerializer"""
	class Meta:
		model = Post
		fields = ('title',"content",
					"time_stamp",
					"photo",
					"published",
					"author_name",
					"category_name")