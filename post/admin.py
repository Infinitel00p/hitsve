from django.contrib import admin

# Register your models here.
from .models import *

class AdminPost(admin.ModelAdmin):
    list_display = ['title','slug','time_stamp']
    list_filter = ["time_stamp"]
    search_fields = ["title"]
    class Meta:
        model = Post
    
admin.site.register(Post, AdminPost)

class AdminCategory(admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ["name"]
    class Meta:
        model = Category
    
admin.site.register(Category, AdminCategory)


class AdminComment(admin.ModelAdmin):
    search_fields = ["id"]
    class Meta:
        model = Comment
    
admin.site.register(Comment, AdminComment)