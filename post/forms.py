from django import forms
from .models import *
from django.forms.widgets import Widget

class CategoryForm (forms.ModelForm):
    
    
    class Meta:
        model = Category
        
        field = (
                'name',
            )
        
        labels = {
                'name':'Nombre',
    
            }
    
        widgets = {
            'name' : forms.TextInput(attrs = {'class':'form'}),
            }
        
        exclude = ['id']
        
        
    def clean_name(self):
        name = self.cleaned_data.get('name')

        if Category.objects.filter(name=name).exists():
            raise forms.ValidationError("Este nombre de categoría ya esta en uso.")

        return name
    
#from ckeditor_uploader.widgets import CKEditorUploadingWidget
from ckeditor.widgets import CKEditorWidget
from ckeditor.fields import RichTextFormField
class PostForm (forms.ModelForm):
    
    
    class Meta:
        model = Post
        
        field = (
                'title',
                'content',
                'photo',
                'category',
                'tags',
            )
        
        labels = {
                'title' : 'Título',
                'content' : 'Cuerpo',
                'photo': 'Portada',
                'category' : 'Categoría',
                'tags' : 'tags'
            }
    
        widgets = {
            'title' : forms.TextInput(attrs = {'class':'form-group'}),
            'content' : forms.CharField(widget=CKEditorWidget()),#attrs = {'class':'mi clases'}
            'photo' : forms.FileInput(attrs = {'class':'form-group'}),
            'category' : forms.Select(attrs = {'class':'select-form'}),
            'tags' : forms.TextInput(attrs = {'class':'form-group'}),
            
            }
        
        exclude = ['published','id','time_stamp', 'Update', 'author', 'likes', 'slug']

    def clean_category(self):
        category = self.cleaned_data.get('category')
        
        if category is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return category

    def clean_photo(self):
        photo = self.cleaned_data.get('photo')
        
        if photo is None:
            raise forms.ValidationError("Este campo es obligatorio")
    
        return photo
        
    """
    def clean_name(self):
        name = self.cleaned_data.get('name')

        if Category.objects.filter(name=name).exists():
            raise forms.ValidationError("Este nombre de categoria ya esta en uso.")

        return name
    
    """
    
class CommentForm (forms.ModelForm):
    
    
    class Meta:
        model = Comment
        
        field = (
                'content',
            )
        
        labels = {
                'content':'Comentar',
    
            }
    
        widgets = {
            'content' : forms.TextInput(attrs = {'class':'form-group'}),
            }
        
        exclude = ['id', 'time_stamp', 'author', 'post']