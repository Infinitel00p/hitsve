"""hits URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from .views import *
from django.contrib.auth.decorators import permission_required, login_required

urlpatterns = [
    #url(r'^(?P<slug>[-\w]+)/(?P<id>\d+)/$', myview_example, name='myview_example')
    url(r'^categorias/$', permission_required('post.add_category')(ListCategory.as_view()), name='listar_categorias'),
    url(r'^crear-categoria/$', permission_required('post.add_category')(CreateCategory.as_view()), name='crear_categoria'),
    url(r'^mis-noticias/$', permission_required('post.add_post')(ListPost.as_view()), name='listar_noticias'),
    url(r'^nueva/$', permission_required('post.add_post')(CreatePost.as_view()), name='crear_noticia'),
    url(r'^editar/(?P<id>\d+)/$', permission_required('post.change_post')(updatePost), name='editar_noticia'),
    url(r'^eliminar/(?P<pk>\d+)/$', permission_required('post.delete_post')(DeletePost.as_view()), name='eliminar_noticia'), 

    url(r'^(?P<slug>[\w-]+)/$', detail, name='detail_noticia'),

    url(r'^ajax/comentar$', login_required(comment), name='comment_noticia'),
    url(r'^ajax/paginar$', comment_pag, name='comment_paginar'),


    url(r'^ajax/likepost$', login_required(like_post), name='like_post'),
    url(r'^ajax/viewpost$', view_post, name='view_post'),

    # api rest
    url(r'api/posts/', PostsList.as_view(), name='pos_posts'),
    url(r'api/search/', PostsSearch.as_view(), name='pos_search'),

]